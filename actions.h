/****************************************************************************/
/* Copyright 2010 MBARI.                                                    */
/* MBARI Proprietary Information. All rights reserved.                      */
/****************************************************************************/

#ifndef ACTIONS_H
#define ACTIONS_H

#include "modbus_registers.h"
#include "AD567x.h"

/* Root commands */
int actGetCmd(char* s);
int actSetCmd(char* s);
int actClrCmd(char* s);
int actHelpCmd(char* s);
int actResetCmd(char* s);

/* debug command(s) below */
int actTest1Cmd(char* s);
int actTest2Cmd(char* s);
int actTest3Cmd(char* s);
int actTest4Cmd(char* s);
int actTest5Cmd(char* s);

int actHelloKittyCmd(char* s);
                                                
/* Get commands */
int actGetStubCmd(char* s);

int actGetPortACmd(char* s);
int actGetPortBCmd(char* s);
int actGetEnvCmd(char* s);
int actGetInst0Cmd(char* s);
int actGetInst1Cmd(char* s);
int actGetIdCmd(char* s);
int actGetBaudCmd(char* s);
int actGetOutDfltCmd(char* s);
int actGetConfigCmd(char* s);
int actGetDINCmd(char * s);
int actGetAINCmd(char * s);

/* Set commands */
int actSetStubCmd(char* s);

int actSetPortACmd(char* s);
int actSetPortBCmd(char* s);
int actSetIdCmd(char* s);
int actSetBaudCmd(char* s);
int actSetOutDfltCmd(char* s);
int actSetDO(char* s, MB_COIL coil);
int actSetLT0Cmd(char* s);
int actSetLT1Cmd(char* s);
int actSetLT2Cmd(char* s);
int actSetLT3Cmd(char* s);
int actSetLT4Cmd(char* s);
int actSetLT5Cmd(char* s);
int actSetIN0Cmd(char* s);
int actSetIN1Cmd(char* s);
int actSetDO0Cmd(char* s);
int actSetDO1Cmd(char* s);
int actSetGFLCmd(char* s);
int actSetGFHCmd(char* s);
int actSetVicorCmd(char* s);
int actSetAO(char *s, AD567X_CHAN channel);
int actSetAO0Cmd(char* s);
int actSetAO1Cmd(char* s);
int actSetAO2Cmd(char* s);
int actSetAO3Cmd(char* s);
int actSetAO4Cmd(char* s);
int actSetAO5Cmd(char* s);
int actSetAO6Cmd(char* s);
int actSetAO7Cmd(char* s);
int actSetInstCfg(int port, char *s);
int actSetInst0Cfg(char*s);
int actSetInst1Cfg(char*s);

/* Clear commands */
int actClrStubCmd(char* s);

#endif
