# MiniROV Light Controller #

The MiniROV light controller is a repo for the firmware using MPLABX IDE version 3.20, and compiles using the XC16 compiler version 1.25. 

Please contact Eric Martin x1862 with any questions regarding this firmware. 

##TODO's

- [] Test against lights (Chad & Eric)
- [] Pressure Test Board (Chad)
- [] Implement config and storage. Serial port setting are static right now. 

## Modbus Addresses ##

See modbus_registers.h