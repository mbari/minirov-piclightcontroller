/****************************************************************************/
/* Copyright 2009 MBARI.                                                    */
/* MBARI Proprietary Information. All rights reserved.                      */
/****************************************************************************/
#include "buffer.h"

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

int bufPut(unsigned char b, ByteBuffer* buff)
{
    if ( bufIsFull(buff) )
    {
        return FALSE;
    }   
    else
    {   
        buff->data[buff->head] = b;
        buff->head = (buff->head + 1) & buff->ptr_mask;
        return TRUE;
    }
}

int bufGet(unsigned char* b, ByteBuffer* buff)
{
    if ( bufIsEmpty(buff) )
    {
        *b = 0;
        return FALSE;
    }
    else
    {
        *b = buff->data[buff->tail];
        buff->tail = (buff->tail + 1) & buff->ptr_mask;
        return TRUE;
    }
}

void bufClear(ByteBuffer* buff)
{
    buff->tail = 0;
    buff->head = 0;
}

int bufIsFull(ByteBuffer* buff)
{
    if ( ((buff->head + 1) & buff->ptr_mask) == buff->tail )
        return TRUE;
    else
        return FALSE;
}

int bufIsEmpty(ByteBuffer* buff)
{
    if (buff->tail == buff->head) 
        return TRUE;
    else
        return FALSE;
}

int bufAvailable(ByteBuffer* buff)
{
    return (buff->head - buff->tail) & buff->ptr_mask;
}

