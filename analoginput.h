/* 
 * File: analoginput.h   
 * Author: E. Martin
 * Comments: File for minirov light controller analog inputs
 * Revision history: Git via BitBucket
 */

#ifndef ANALOG_INPUT_H
#define	ANALOG_INPUT_H

#include "modbus_registers.h"

/* Please refer to modbus_registers.h if these values need to change.*/
#define ANALOG_REG_START ANALOG_IN_0 
#define ANALOG_REG_END GF_CURRENT

/**
* ADC1_CHANNEL
*
* values of valid channels
*/
typedef enum {
	ADC1_CHANNEL_AN0 = 0x06,
	ADC1_CHANNEL_AN1 = 0x07,
	ADC1_CHANNEL_AN2 = 0x08,
	ADC1_CHANNEL_AN3 = 0x09,
    ADC1_AMB_TEMP = 0x0A,
    ADC1_GF_CURRENT=0x0B
} ADC1_CHANNEL;

/**
 * unsigned int analogReadReg(unsigned int address);
 * @param address : modbus register address. 
 * @return 
 */
unsigned int analogReadReg(unsigned int address);

/**
 * analogInit();
 * 
 * This should be run to configure the ADC1 module appropriately
 * for self timed smart sampling.
 */  
void analogInit();

/**
 * read adc channel and return value
 * 
 * @param channel 
 * @return 
 */
unsigned int analogReadADC(ADC1_CHANNEL channel);
#endif	/* ANALOG_INPUT_H */

