/****************************************************************************/
/* Copyright 2010 MBARI.                                                    */
/* MBARI Proprietary Information. All rights reserved.                      */
/****************************************************************************/

#include <p24fxxxx.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "actions.h"
#include "errors.h"
#include "parser.h"
#include "config.h"
#include "serial.h"
#include "dig_out.h"
#include "dig_in.h"
#include "analoginput.h"
#include "env.h"
#include "sys_defs.h"

static char msg_buff[64];
//static ConfigData configData;

/****************************************************************************/
/*                               Root commands                              */
/****************************************************************************/
int actGetCmd(char* s)
{
    return prsParse(s, cmdsGet, ERR_NO_SUB_COMMAND);
}

int actSetCmd(char* s)
{
    return prsParse(s, cmdsSet, ERR_NO_SUB_COMMAND);
}

int actClrCmd(char* s)
{
    return prsParse(s, cmdsClr, ERR_NO_SUB_COMMAND);
}

int actHelpCmd(char* s)
{
    serPutString(CONSOLE_PORT, "HELP - display this menu\r\n");
    serPutString(CONSOLE_PORT, "RESET - reset the controller\r\n");
    serPutString(CONSOLE_PORT, "\r\n");
//    serPutString(CONSOLE_PORT, "GET OUTPUT - get output register 0x0001\r\n");
//    serPutString(CONSOLE_PORT, "GET ENV - get the environmental data\r\n");
//    serPutString(CONSOLE_PORT, "GET ID - get the modbus slave ID\r\n");
    serPutString(CONSOLE_PORT, "GET DIN - get status of all DIN Channels\r\n");
    serPutString(CONSOLE_PORT, "GET AIN - get status of the 4 AIN Channels\r\n");
    serPutString(CONSOLE_PORT, "GET INST0 - get status of the first instrument port \r\n");
    serPutString(CONSOLE_PORT, "GET INST1 - get status of the second instrument port \r\n");
//    serPutString(CONSOLE_PORT, "GET BAUD - get the modbus baud rate\r\n");
//    serPutString(CONSOLE_PORT, "GET OUTDFLT - get the output reset state\r\n");
//    serPutString(CONSOLE_PORT, "GET CONFIG - get all config parameters\r\n");
    serPutString(CONSOLE_PORT, "GET AI - read all channels of enable analog input.\r\n");
    serPutString(CONSOLE_PORT, "\r\n");
//    serPutString(CONSOLE_PORT, "SET OUTPUT - set output register 0x0001\r\n");
//    serPutString(CONSOLE_PORT, "SET ID - set the modbus slave ID\r\n");
//    serPutString(CONSOLE_PORT, "SET BAUD - set the modbus baud rate\r\n");
//    serPutString(CONSOLE_PORT, "SET OUTDFLT- set the output reset state\r\n");
    serPutString(CONSOLE_PORT, "SET LT<x> - set the on/off [1/0] state of the <x> (0:5) light switches\r\n");
    serPutString(CONSOLE_PORT, "SET IN<x> - set the on/off [1/0] state of the <x> (0:1) instrument switches\r\n");
    serPutString(CONSOLE_PORT, "SET VICOR - set the on/off [1/0] state of the vicor enable line.\r\n");
    serPutString(CONSOLE_PORT, "SET DO<x> - set the on/off [1/0] state of DO<x> (0:1)\r\n");
    serPutString(CONSOLE_PORT, "SET GFL - set the on/off state [1/0] of the low side GF Test\r\n");
    serPutString(CONSOLE_PORT, "SET GFH - set the on/off state [1/0] of the high side GF Test\r\n");
    serPutString(CONSOLE_PORT, "SET AO<x> - set the 12-bit value of the AO<x> (0:7)\r\n"); 
    serPutString(CONSOLE_PORT, "SET INST<x> <baud>,<bits>,<parity>,<stop> - set the serial port settings of the port.\r\n"); 

    return ERR_SUCCESS;
}

int actResetCmd(char* s)
{
    asm("reset");
    return ERR_SUCCESS;
}

int actTest1Cmd(char* s)
{
   serPutString(CONSOLE_PORT, "TEST1\r\n");
#define FCY_TEST  1843200


   unsigned long baud = atol(s);
   unsigned int brg_reg;

#define BRGH_BIT_VAL 1

   if ( BRGH_BIT_VAL )
      brg_reg = ( FCY / ( 4 * baud )) - 1;
   else
      brg_reg = ( FCY / ( 16 * baud )) - 1;



   sprintf(msg_buff, "baud    = %lu\r\n", baud);
   serPutString(CONSOLE_PORT, msg_buff);
   sprintf(msg_buff, "brg_reg = %u\r\n", brg_reg);
   serPutString(CONSOLE_PORT, msg_buff);

   /*********************************************/
   /*           Baud Rate Calculation           */
   /*********************************************/
   /*                                           */
   /* BRGH = 0                                  */
   /*                                           */
   /*   UxBRG = ( Fcy / ( 16 * baud )) - 1      */
   /*                                           */
   /* BRGH = 1                                  */
   /*                                           */
   /*   UxBRG = ( Fcy / ( 4 * baud )) - 1       */
   /*                                           */
   /*   Current Settings                        */
   /*   Fosc = 3.6864 MHz                       */
   /*   Fcy = Fosc / 2                          */
   /*   BRGH = 1                                */
   /*                                           */
   /*    ___________________________            */
   /*   |  Baud          |  UxBRG  |            */
   /*   ----------------------------            */
   /*   |  9600         |   47     |            */
   /*   ----------------------------            */
   /*   |  19200        |   23     |            */
   /*   ----------------------------            */
   /*   |  38400        |   11     |            */
   /*   ----------------------------            */
   /*   |  57600        |   7      |            */
   /*   ----------------------------            */
   /*   |  115200       |   3      |            */
   /*   ---------------------------             */
   /*                                           */
   /*********************************************/


   return ERR_SUCCESS;
}

int actTest2Cmd(char* s)
{
   serPutString(CONSOLE_PORT, "TEST2\r\n");

   return ERR_SUCCESS;
}

int actTest3Cmd(char* s)
{
   serPutString(CONSOLE_PORT, "TEST3\r\n");

   return ERR_SUCCESS;
}

int actTest4Cmd(char* s)
{
   serPutString(CONSOLE_PORT, "TEST4\r\n");

   return ERR_SUCCESS;
}

int actTest5Cmd(char* s)
{
   serPutString(CONSOLE_PORT, "TEST5\r\n\r\n");

   return ERR_SUCCESS;
}

int actHelloKittyCmd(char* s)
{

#if 0
/* hidden help */
int actHelloKittyCmd(char* s, int show_help)
    
    if ( show_help )
        return ERR_SUCCESS;
#endif


    serPutString(CONSOLE_PORT, "MEOW\r\n");
    return ERR_SUCCESS;
}

/****************************************************************************/
/*                               Get commands                               */
/****************************************************************************/
int actGetStubCmd(char* s)
{
    sprintf(msg_buff, "actGetStubCmd(%s)\r\n", s);
    serPutString(CONSOLE_PORT, msg_buff);

    return ERR_SUCCESS;
}

int actGetInst0Cmd(char *s) 
{
    //get serial port settings

    unsigned long baud;
    int data_par; 
    int stop;
    serGetConfig(INST_PORT_0, &baud, &data_par, &stop);

    // step through each type and print to the port
    serPutString(CONSOLE_PORT, "Instrument Port 0 Settings: \r\n");

    //baud
    sprintf(msg_buff,"\tBaud: %lu\r\n",baud);
    serPutString(CONSOLE_PORT, msg_buff);

    //data and parity
    switch ( data_par ) 
    {
        case D8_NONE: serPutString(CONSOLE_PORT, "\tData & Parity: 8 NONE\r\n"); break;
        case D8_EVEN: serPutString(CONSOLE_PORT, "\tData & Parity: 8 EVEN\r\n"); break;
        case D8_ODD:  serPutString(CONSOLE_PORT, "\tData & Parity: 8 ODD\r\n"); break;
        case D9_NONE: serPutString(CONSOLE_PORT, "\tData & Parity: 9 NONE\r\n"); break;
        default: serPutString(CONSOLE_PORT, "!!! ERROR READING DATA & PARITY\r\n"); 
    }

    //stop bits   
    switch (stop) {
        case 1:serPutString(CONSOLE_PORT,"\tStop Bits: 1\r\n");break;
        case 2:serPutString(CONSOLE_PORT,"\tStop Bits: 2\r\n");break;
    }

    return ERR_SUCCESS;
}

int actGetInst1Cmd(char *s) 
{
    //get serial port settings

    unsigned long baud;
    int data_par; 
    int stop;
    serGetConfig(INST_PORT_1, &baud, &data_par, &stop);

    // step through each type and print to the port
    serPutString(CONSOLE_PORT, "Instrument Port 1 Settings: \r\n");

    //baud
    sprintf(msg_buff,"\tBaud: %lu\r\n",baud);
    serPutString(CONSOLE_PORT, msg_buff);

    //data and parity
    switch ( data_par ) 
    {
        case D8_NONE: serPutString(CONSOLE_PORT, "\tData & Parity: 8 NONE\r\n"); break;
        case D8_EVEN: serPutString(CONSOLE_PORT, "\tData & Parity: 8 EVEN\r\n"); break;
        case D8_ODD:  serPutString(CONSOLE_PORT, "\tData & Parity: 8 ODD\r\n"); break;
        case D9_NONE: serPutString(CONSOLE_PORT, "\tData & Parity: 9 NONE\r\n"); break;
        default: serPutString(CONSOLE_PORT, "!!! ERROR READING DATA & PARITY\r\n"); 
    }

    //stop bits   
    switch (stop) {
        case 1:serPutString(CONSOLE_PORT,"\tStop Bits: 1\r\n");break;
        case 2:serPutString(CONSOLE_PORT,"\tStop Bits: 2\r\n");break;
    }

    return ERR_SUCCESS;
}

//int actGetPortACmd(char* s)
//{
//   unsigned int port_val;
//   doRead(1, &port_val);
//
//   sprintf(msg_buff, "PORT A: 0x%04X\r\n", port_val);
//   serPutString(CONSOLE_PORT, msg_buff);
//
//   return ERR_SUCCESS;
//}

//int actGetPortBCmd(char* s)
//{
//   unsigned int port_val;
//   doRead(2, &port_val);
//
//   sprintf(msg_buff, "PORT B: 0x%04X\r\n", port_val);
//   serPutString(CONSOLE_PORT, msg_buff);
//
//   return ERR_SUCCESS;
//}
//
//int actGetEnvCmd(char* s)
//{
//   sprintf(msg_buff, "TEMPERATURE = %4.1f C\r\n", envGetTemp());
//   serPutString(CONSOLE_PORT, msg_buff);
//
//   sprintf(msg_buff, "HUMIDITY    = %4.1f %%\r\n", envGetHum());
//   serPutString(CONSOLE_PORT, msg_buff);
//
//   sprintf(msg_buff, "STATUS BITS = 0x%02X\r\n", envGetStat());
//   serPutString(CONSOLE_PORT, msg_buff);
//
//   return ERR_SUCCESS;
//}
//
//int actGetIdCmd(char* s)
//{
//   /* read the current config */
//   cfgRead(&configData);
//
//   sprintf(msg_buff, "MODBUS ID: %u\r\n", configData.modbusId);
//   serPutString(CONSOLE_PORT, msg_buff);
//
//   return ERR_SUCCESS;
//}
//
//int actGetBaudCmd(char* s)
//{
//   /* read the current config */
//   cfgRead(&configData);
//
//   sprintf(msg_buff, "MODBUS BAUD: %lu\r\n", configData.modbusBaud);
//   serPutString(CONSOLE_PORT, msg_buff);
//
//   return ERR_SUCCESS;
//}
//
//int actGetOutDfltCmd(char* s)
//{
//   /* read the current config */
//   cfgRead(&configData);
//
//   sprintf(msg_buff, "OUTPUT DEFAULT: 0x%08lX\r\n", configData.pwrUpDigOut);
//   serPutString(CONSOLE_PORT, msg_buff);
//
//   return ERR_SUCCESS;
//}
//
//int actGetConfigCmd(char* s)
//{
//   /* read the current config */
//   cfgRead(&configData);
//
//   sprintf(msg_buff, "MODBUS ID     : %u\r\n", configData.modbusId);
//   serPutString(CONSOLE_PORT, msg_buff);
//
//   sprintf(msg_buff, "MODBUS BAUD   : %lu\r\n", configData.modbusBaud);
//   serPutString(CONSOLE_PORT, msg_buff);
//
//   sprintf(msg_buff, "OUTPUT DEFAULT: 0x%08lX\r\n", configData.pwrUpDigOut);
//   serPutString(CONSOLE_PORT, msg_buff);
//
//   return ERR_SUCCESS;
//}

int actGetDINCmd(char * s){ 

    sprintf(msg_buff, "DI0: %u\r\n",IO_DigitalIn0_GetValue());
    serPutString(CONSOLE_PORT, msg_buff);
    sprintf(msg_buff, "DI1: %u\r\n",IO_DigitalIn0_GetValue());
    serPutString(CONSOLE_PORT, msg_buff);
    sprintf(msg_buff, "CNIN_O: %u\r\n",IO_CNIn0_GetValue()); 
    serPutString(CONSOLE_PORT, msg_buff);
    sprintf(msg_buff, "CNIN_1: %u \r\n",IO_CNIn1_GetValue());
    serPutString(CONSOLE_PORT, msg_buff);
    sprintf(msg_buff, "H20_SENSE: %u\r\n",IO_H2OSense_GetValue());
    serPutString(CONSOLE_PORT, msg_buff);
    sprintf(msg_buff, "UART0_MODE: %u\r\n",IO_Uart0Mode_GetValue());
    serPutString(CONSOLE_PORT, msg_buff);
    sprintf(msg_buff, "ADDR_0: %u\r\n",IO_BoardAddr0_GetValue());
    serPutString(CONSOLE_PORT, msg_buff);
    sprintf(msg_buff, "ADDR_1: %u\r\n",IO_BoardAddr1_GetValue());
    serPutString(CONSOLE_PORT, msg_buff);
    sprintf(msg_buff, "ADDRESS: %u\r\n",hwBoardAddress());
    serPutString(CONSOLE_PORT, msg_buff);

    return ERR_SUCCESS;
}
int actGetAINCmd(char * s){ 

    unsigned int iAnalogValue = 0;

    //test AIN0
    iAnalogValue = analogReadReg(ANALOG_IN_0);
    sprintf(msg_buff, "AIN ANALOG_IN_0: %u\r\n",iAnalogValue);
    serPutString(CONSOLE_PORT,msg_buff);

    //test AIN1
    iAnalogValue = analogReadReg(ANALOG_IN_1);
    sprintf(msg_buff, "AIN ANALOG_IN_1: %u\r\n",iAnalogValue);
    serPutString(CONSOLE_PORT,msg_buff);

    //test AIN2
    iAnalogValue = analogReadReg(ANALOG_IN_2);
    sprintf(msg_buff, "AIN ANALOG_IN_2: %u\r\n",iAnalogValue);
    serPutString(CONSOLE_PORT,msg_buff);

    //test AIN3
    iAnalogValue = analogReadReg(ANALOG_IN_3);
    sprintf(msg_buff, "AIN ANALOG_IN_3: %u\r\n",iAnalogValue);
    serPutString(CONSOLE_PORT,msg_buff);

    //test AMB_TEMP
    iAnalogValue = analogReadReg(AMB_TEMP);
    sprintf(msg_buff, "AIN AMB_TEMP: %u\r\n",iAnalogValue);
    serPutString(CONSOLE_PORT,msg_buff);

    //test GF_CURRENT
    iAnalogValue = analogReadReg(GF_CURRENT);
    sprintf(msg_buff, "AIN GF_CURRENT: %u\r\n",iAnalogValue);
    serPutString(CONSOLE_PORT,msg_buff);

    return ERR_SUCCESS;
}

/****************************************************************************/
/*                               Set commands                               */
/****************************************************************************/
int actSetStubCmd(char* s)
{
    sprintf(msg_buff, "actSetStubCmd(%s)\r\n", s);
    serPutString(CONSOLE_PORT, msg_buff);

    return ERR_SUCCESS;
}

//int act1etPortACmd(char* s)
//{
//    unsigned int port_val = (unsigned int)strtoul(s, 0, 16);;
//    doWrite(1, port_val);
//
//    sprintf(msg_buff, "PORT A: 0x%04X\r\n", port_val);
//    serPutString(CONSOLE_PORT, msg_buff);
//
//    return ERR_SUCCESS;
//}
//
//int actSetPortBCmd(char* s)
//{
//   unsigned int port_val = (unsigned int)strtoul(s, 0, 16);;
//   doWrite(2, port_val);
//
//   sprintf(msg_buff, "PORT B: 0x%04X\r\n", port_val);
//   serPutString(CONSOLE_PORT, msg_buff);
//
//   return ERR_SUCCESS;
//}


//int actSetIdCmd(char* s)
//{
//   unsigned int modbus_id;
//
//   /* read the current config */
//   cfgRead(&configData);
//
//   /* check the range of the value */
//   modbus_id = atoi(s);
//
//   if ( (modbus_id < 1) || (modbus_id > 99) )
//   {
//      serPutString(CONSOLE_PORT, "MODBUS ID RANGE ERR [1-99]\r\n");
//      return ERR_SUCCESS;
//   }
//
//   /* set the new config value*/
//   configData.modbusId = modbus_id;
//
//   sprintf(msg_buff, "MODBUS ID: %u\r\n", configData.modbusId);
//   serPutString(CONSOLE_PORT, msg_buff);
//
//   /* write the modified config */
//   cfgWrite(&configData);
//
//   return ERR_SUCCESS;
//}
//
//int actSetBaudCmd(char* s)
//{
//   unsigned long baud;
//
//   /* read the current config */
//   cfgRead(&configData);
//
//   /* check the range of the value */
//   baud = atol(s);
//
//   switch ( baud )
//   {
//      case CFG_MODBUS_BAUD_9600: break;
//      case CFG_MODBUS_BAUD_19200: break;
//      case CFG_MODBUS_BAUD_38400: break;
//      case CFG_MODBUS_BAUD_57600: break;
//      case CFG_MODBUS_BAUD_115200: break;
//      default: serPutString(CONSOLE_PORT, "MODBUS BAUD INVALID "
//                            "[9600, 19200, 38400, 57600, 115200]\r\n");
//               return ERR_SUCCESS;
//   }
//
//   /* set the new config value*/
//   configData.modbusBaud = baud;
//
//   sprintf(msg_buff, "MODBUS BAUD: %lu\r\n", configData.modbusBaud);
//   serPutString(CONSOLE_PORT, msg_buff);
//
//   /* write the modified config */
//   cfgWrite(&configData);
//
//   return ERR_SUCCESS;
//}
//
//int actSetOutDfltCmd(char* s)
//{
//   unsigned long pwr_up_dflt;
//
//   /* read the current config */
//   cfgRead(&configData);
//
//   /* set the new config value*/
//   pwr_up_dflt = strtoul(s, 0, 16);
//   configData.pwrUpDigOut = pwr_up_dflt;
//
//   sprintf(msg_buff, "OUTPUT DEFAULT: 0x%08lX\r\n", configData.pwrUpDigOut);
//   serPutString(CONSOLE_PORT, msg_buff);
//
//   /* write the modified config */
//   cfgWrite(&configData);
//
//   return ERR_SUCCESS;
//}
//
int actSetDO(char *s, MB_COIL coil) {

    unsigned long outval;

    // Get the new value;
    outval = strtoul(s,0,16);

    if (outval ==1) 
        doSetBit(coil,(int)outval);
    else if ( outval == 0 ) 
        doSetBit(coil,(int)outval);
    else return ERR_FAILURE;

    return ERR_SUCCESS;

}

int actSetLT0Cmd(char* s){
    return actSetDO(s,ISOPWR_CNTL_0);
}
int actSetLT1Cmd(char* s){
    return actSetDO(s,ISOPWR_CNTL_1);
}
int actSetLT2Cmd(char* s){
    return actSetDO(s,ISOPWR_CNTL_2);
}
int actSetLT3Cmd(char* s){
    return actSetDO(s,ISOPWR_CNTL_3);
}
int actSetLT4Cmd(char* s){
    return actSetDO(s,ISOPWR_CNTL_4);
}
int actSetLT5Cmd(char* s){
    return actSetDO(s,ISOPWR_CNTL_5);
}
int actSetIN0Cmd(char* s){
    return actSetDO(s,INSTR_CNTL_0);
}
int actSetIN1Cmd(char* s){
    return actSetDO(s,INSTR_CNTL_1);
}
int actSetDO0Cmd(char* s){
    return actSetDO(s,DIGITAL_OUT_0);
}
int actSetDO1Cmd(char* s){
    return actSetDO(s,DIGITAL_OUT_1);
}
int actSetGFLCmd(char* s){
    return actSetDO(s,GF_CNTL_LOW);
}
int actSetVicorCmd(char* s){
    return actSetDO(s,VICOR_CNTL);
}
int actSetGFHCmd(char* s){
    return actSetDO(s,GF_CNTL_HIGH);
}
int actSetAO(char *s, AD567X_CHAN channel) {

    unsigned long outval;
    unsigned int dacVal = 0;

    // Get the new value;
    outval = strtoul(s,0,10);

    AD567X_WriteToAndUpdateChannel(channel, (unsigned short) outval);

    // Now readback state
    
    dacVal = AD567X_ReadBackEnable(channel);

    Nop(); 
    
    sprintf(msg_buff, "OUTPUT = %u\r\n",dacVal);
    serPutString(CONSOLE_PORT,msg_buff);

    return ERR_SUCCESS;
}
int actSetAO0Cmd(char* s){
    return actSetAO(s,DACCHAN_0);
}
int actSetAO1Cmd(char* s){
    return actSetAO(s,DACCHAN_1);
}
int actSetAO2Cmd(char* s){
    return actSetAO(s,DACCHAN_2);
}
int actSetAO3Cmd(char* s){
    return actSetAO(s,DACCHAN_3);
}
int actSetAO4Cmd(char* s){
    return actSetAO(s,DACCHAN_4);
}
int actSetAO5Cmd(char* s){
    return actSetAO(s,DACCHAN_5);
}
int actSetAO6Cmd(char* s){
    return actSetAO(s,DACCHAN_6);
}
int actSetAO7Cmd(char* s){
    return actSetAO(s,DACCHAN_7);
}

int actSetInst0Cfg(char*s)  { return actSetInstCfg(INST_PORT_0,s); }
int actSetInst1Cfg(char*s)  { return actSetInstCfg(INST_PORT_1,s); }

int actSetInstCfg(int port, char *s)
{   
    unsigned long baud;
    int cfgDataPar,cfgStop;
    char data, par, stop;
    char *tok;
    const char *delim = ",";
    ConfigData configData;

    tok = strtok(s,delim);
    baud = strtoul(tok,0,10);
    tok = strtok(NULL,delim);
    data = tok[0];
    tok = strtok(NULL,delim);
    par = tok[0];
    tok = strtok(NULL,delim);
    stop = tok[0];

    sprintf(msg_buff,"Setting Instrument %i to %lu, %c, %c, %c\r\n",port, baud, data, par, stop);
    serPutString(CONSOLE_PORT,msg_buff);

    //first translate chars to config values
    /* Data Bits & Parity */
    if  ( data == '8' ) {
            switch ( par ) {
                case 'N':
                case 'n':
                    cfgDataPar = CFG_INST_PAR_8NONE; break;
                case 'E':
                case 'e':
                    cfgDataPar = CFG_INST_PAR_8EVEN; break;
                case 'O':
                case 'o':
                    cfgDataPar = CFG_INST_PAR_8ODD; break;
                default: return ERR_FAILURE;
            }
    }
    else if ( data == '9' ) {
        if ( (par == 'N') || (par == 'n') ) {
            cfgDataPar = CFG_INST_PAR_9NONE;
        }
        else return ERR_FAILURE;
    }
    else return ERR_FAILURE;

    /* Stop Bit */
    if ( stop == '1' )
        cfgStop = CFG_INST_STOP_ONE;
    else if ( stop == '2' )
        cfgStop = CFG_INST_STOP_TWO;
    else 
        return ERR_FAILURE;

    /* We don't check baud, attempting to set the baud rate will check that. */

    // We made it this far, time to pull the config data

    cfgRead(&configData);
    
    //now change
    switch (port) {
        case INST_PORT_0: 
            configData.inst0Baud = baud;
            configData.inst0Parity = cfgDataPar;
            configData.inst0Stop = cfgStop;
            break;
        case INST_PORT_1:
            configData.inst1Baud = baud;
            configData.inst1Parity = cfgDataPar;
            configData.inst1Stop = cfgStop;
            break;
        default: return ERR_FAILURE;
    }

    //write changes
    if ( cfgWrite(&configData) == 0 ) //success
    {   
        sprintf(msg_buff, "New settings (%s) on port %i. Reset to take effect.\r\n",s,port); 
        serPutString(CONSOLE_PORT,msg_buff);
    }
    else {
        sprintf(msg_buff, "Failed write, not a known baud value (%s) .\n\r",s);
        serPutString(CONSOLE_PORT,msg_buff);
        return ERR_FAILURE;
    }


    return ERR_SUCCESS;
}



/****************************************************************************/
/*                              Clear commands                              */
/****************************************************************************/
int actClrStubCmd(char* s)
{
    sprintf(msg_buff, "actClrStubCmd(%s)\r\n", s);
    serPutString(CONSOLE_PORT, msg_buff);

    return ERR_SUCCESS;
}

