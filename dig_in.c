/* 
 * File: dig_in.c
 * Author: E. Martin
 * Comments: 	digital input file, which covers access to four signals
 * 		on the target board, which at this point are define as 
 *		generic signals in the modbus address definitions.
 *    
 * Revision history: Git via BitBucket 
 * 		https://bitbucket.org/mbari/minirov-piclightcontroller 
 * 
 * Copyright: 2016 MBARI, this is proprietary code, all rights reserved.
 *
 */

#include <xc.h>
#include "dig_in.h"
#include "modbus_registers.h"


int discreteReadReg(unsigned int address) {

	// check address is valid
	if (address < REG_DISCRETE_START ||
		 address > REG_DISCRETE_START + REG_DISCRETE_NREGS)
	{
		return -1;
	}

	switch (address) {

		case DIGITAL_IN_0:
			return IO_DigitalIn0_GetValue();
			break;
		case DIGITAL_IN_1:
			return IO_DigitalIn1_GetValue();
			break;
		case CN_IN_0:
			return IO_CNIn0_GetValue();
			break;
		case CN_IN_1:
			return IO_CNIn1_GetValue();
			break;
		case H2O_SENSE: 
			return IO_H2OSense_GetValue();			
			break;
		default:
			return -1;
	}

}

unsigned int discreteReadAll() {
    unsigned int retval = 0; 


    
    retval |= (((IO_DigitalIn0_GetValue() == 1)? 1:0)<<0);
    retval |= (((IO_DigitalIn1_GetValue() == 1)? 1:0)<<1);
    retval |= (((IO_CNIn0_GetValue()      == 1)? 1:0)<<2);
    retval |= (((IO_CNIn1_GetValue()      == 1)? 1:0)<<3);
    //retval |= (((IO_H2OSense_GetValue()   == 1)? 1:0)<<4);

    return retval;
}

unsigned char hwBoardAddress() {

    unsigned char retval = MBADDR_DEFAULT;
    unsigned char ucSelected = 0;

    /* Read in values */
    
    ucSelected |= (((IO_BoardAddr0_GetValue() == 1)? 1:0) <<0);
    ucSelected |= (((IO_BoardAddr1_GetValue() == 1)? 1:0) <<1);

    /* Determine address */
    
    switch (ucSelected) {
        case 0:
            retval = MBADDR_1;
            break;
        case 1:
            retval = MBADDR_2;
            break;
        case 2:
            retval = MBADDR_3;
            break;
        case 3:
            retval = MBADDR_4;
            break;
        default:
            retval = MBADDR_DEFAULT;
            break;
    }

    return retval;
}
