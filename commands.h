/****************************************************************************/
/* Copyright 2010 MBARI.                                                    */
/* MBARI Proprietary Information. All rights reserved.                      */
/****************************************************************************/

#ifndef COMMANDS_H
#define COMMANDS_H

typedef struct
{
    char* name;
    int (*func)(char *);
} CmdStruct;

extern const CmdStruct cmdsRoot[];
extern const CmdStruct cmdsGet[];
extern const CmdStruct cmdsSet[];
extern const CmdStruct cmdsClr[];

#endif

