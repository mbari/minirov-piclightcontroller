/****************************************************************************/
/* Copyright 2009 MBARI.                                                    */
/* MBARI Proprietary Information. All rights reserved.                      */
/****************************************************************************/
#ifndef SERIAL_H
#define SERIAL_H


#define INST_PORT_0     0
#define INST_PORT_1     1
#define INST_PORT_2     2

/* This is a first cut at making the modbus inst_port code more
   portable.  It uses #define macros to specifiy which UART
   hardware to use.  Sort of kludgy, but will work till I come
   up with something better. */

#define ENABLE_PORT_0   1
#define ENABLE_PORT_1   1
#define ENABLE_PORT_2   0

#define INST_PORT_TOTAL 2

/* data/parity settings */
#define D8_NONE         0
#define D8_EVEN         1
#define D8_ODD          2
#define D9_NONE         3

/* stop bits */
#define S_1             1
#define S_2             2

void serInit(); 

void serSetEnable(int val);
int serGetEnable();

int serSetConfig(int port, unsigned long baud, int data_par, int stop);
int serGetConfig(int port, unsigned long* baud, int* data_par, int* stop);

int serGetByte(int port, unsigned char* b);
int serPutByte(int port, unsigned char b);
int serPutString(int port, char* buff);

void serRxFlush(int port);
void serTxFlush(int port);

void serSetBreak(int port, int state);
int serGetBreak(int port);

int serIsRxFull(int port);
int serIsTxFull(int port);

int serGetErrFlag(int port);

void serClrXERR(int port);
int serGetXERR(int port);

void serClrOERR(int port);
int serGetOERR(int port);

void serClrFERR(int port);
int serGetFERR(int port);

void serClrPERR(int port);
int serGetPERR(int port);

#endif

