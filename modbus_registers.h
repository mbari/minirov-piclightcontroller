/* 
 * File: modbus_registers.h   
 * Author: E. Martin
 *
 * Comments: 	This file outlines the register types and ranges,
 *           	along with macros for individual registers as 
 * 		defined for the MiniROV's PIC Light Controller. 
 * 
 *		What it doesn't contain is the ranges for the 
 *		multiplexed serial port data. Those are enclosed
 *		in their own set of access files. 
 * 
 * Revision history: Git via BitBucket
 * 
 * Copyright MBARI 2016
 * 
 *
 */

#ifndef _MODBUS_REGISTERS_H_
#define _MODBUS_REGISTERS_H_
// Register Ranges
#define REG_COIL_START 0x0001
#define REG_COIL_NREGS 17

#define REG_DISCRETE_START 0x0001
#define REG_DISCRETE_NREGS 5

//#define REG_HOLDING_START 0x0200
//#define REG_HOLDING_NREGS 0x0301
#define REG_HOLDING_COMMON_START 0x0200
#define REG_HOLDING_COMMON_NREGS 9
/*#define REG_HOLDING_UART1_START 0x00FF
#define REG_HOLDING_UART1_NREGS 0x0101
#define REG_HOLDING_UART2_START 0x0200
#define REG_HOLDING_UART2_NREGS 0x0101
*/

/* HOLDING REGS ARE HIGH BECAUSE WE IMPLEMENT THE SERIAL PORT MUXES
 * FOR RECORD, COMMON HOLDING REGS START AT 0X0301 AND
 * SERIAL PORT 1 STARTS AT 0X00FF ENDS AT 0X01FF
 * SERIAL PORT 2 STARTS AT 0X0300 ENDS AT 0X0300
 *
 */

#define REG_INPUT_START 0x0001
#define REG_INPUT_NREGS 7

// Type Definitions for board specific registers. 
typedef enum {
	ANALOG_IN_0 = REG_INPUT_START,
	ANALOG_IN_1,
	ANALOG_IN_2,
	ANALOG_IN_3,
	AMB_TEMP   ,
	GF_CURRENT ,
	DISCRETE_INPUTS
} MB_INPUT_REGISTER;

typedef enum {
	LED_DAC_0 = REG_HOLDING_COMMON_START ,
	LED_DAC_1,
	LED_DAC_2,
	LED_DAC_3,
	LED_DAC_4,
	LED_DAC_5,
	DAC_OUT_6,
	DAC_OUT_7,
	COIL_OUTPUT
} MB_HOLDING_REGISTER;

typedef enum {
	ISOPWR_CNTL_0 = REG_COIL_START,
	ISOPWR_CNTL_1,
	ISOPWR_CNTL_2,
	ISOPWR_CNTL_3,
	ISOPWR_CNTL_4,
	ISOPWR_CNTL_5,
	INSTR_CNTL_0,
	INSTR_CNTL_1,
	DIGITAL_OUT_0,
	DIGITAL_OUT_1,
	GF_CNTL_HIGH,
	GF_CNTL_LOW,
	BLINKY_DEBUG,
	BLINKY_STATE,
	H2O_ALARM_CLR, //0x000F
    VICOR_CNTL, //0x0010
} MB_COIL;

typedef enum {
	DIGITAL_IN_0 = REG_DISCRETE_START,
	DIGITAL_IN_1,
	CN_IN_0,
	CN_IN_1,
	H2O_SENSE
} MB_DISCRETE_INPUT;
#endif
