/****************************************************************************/
/* Copyright 2006 MBARI.                                                    */
/* MBARI Proprietary Information. All rights reserved.                      */
/****************************************************************************/

#ifndef PARSER_H
#define PARSER_H

#include "commands.h"

int prsNameMatch(char* s, char* ref);
char* prsSkip(char* s, char* ignore);
int prsIsInteger(char* txt);
int prsIsValidNameChar(char c);
int prsParse(char* line, const CmdStruct* cmds, int err_code);

#endif
