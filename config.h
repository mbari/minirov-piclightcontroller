/****************************************************************************/
/* Copyright 2012 MBARI.                                                    */
/* MBARI Proprietary Information. All rights reserved.                      */
/****************************************************************************/

#ifndef CONFIG_H
#define CONFIG_H

typedef struct
{
   unsigned int bootFlag;
   unsigned long inst0Baud;
   unsigned char inst0Parity;
   unsigned char inst0Stop;

   unsigned long inst1Baud;
   unsigned char inst1Parity;
   unsigned char inst1Stop;
   
   unsigned long debugBits;
} ConfigData;

int cfgDefault();
int cfgCheck(ConfigData* config);
int cfgRead(ConfigData* config);
int cfgWrite(ConfigData* config);
int cfgSetBootFlag();

#endif

