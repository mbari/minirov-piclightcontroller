/* 
 * File: dig_out.h   
 * Author: E. Martin
 * Comments: 	digital output functions for control via modbus in MiniROV
 *
 *    
 * Revision history: Git via BitBucket 
 * 		https://bitbucket.org/mbari/minirov-piclightcontroller 
 * 
 * Copyright: 2016 MBARI, this is proprietary code, all rights reserved.
 *
 */
#ifndef DIG_OUTPUT_H
#define DIG_OUTPUT_H

/* init registers */
void doInit(); 

/* power bit API */
void doSetBit(unsigned int addr, int state);
int doGetBit(unsigned int bit);

/**
 * Read all outputs and fill a register with the information.
 *
 * void doReadReg(int addr, unsigned short * val );
 *
 * @param addr the relevant DO address of the register. 
 * @param val pointer to the register to fill.  
 */
int doReadReg(unsigned int addr, unsigned short * val );

/* misc. */
void doSetBlinky(int state);
void doToggleBlinky();
int doGetBlinky();

#endif
