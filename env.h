/* 
 * File: env.h   
 * Author: E. Martin
 * Comments: environment functions file, which for this implementation
 * 		controls the leak sensor and the ground fault current 
 * 		sensor functionality. 
 *  
 * Revision history: Git via BitBucket 
 * 		https://bitbucket.org/mbari/minirov-piclightcontroller 
 * 
 * Copyright: 2016 MBARI, this is proprietary code, all rights reserved.
 *
 */

#ifndef _ENV_H_
#define _ENV_H_
	


#endif
