/****************************************************************************/
/* Copyright 2011 MBARI.                                                    */
/* MBARI Proprietary Information. All rights reserved.                      */
/****************************************************************************/
#ifndef SYS_DEFS_H
#define SYS_DEFS_H

/* oscillator freq */
#define XTFREQ       8000000              /* On-board Crystal frequency   */
#define PLLMODE      1                    /* On-chip PLL setting          */
#define FOSC         (XTFREQ * PLLMODE)   /* Internal clock frequency     */
#define FCY          (FOSC / 2)           /* Instruction Cycle frequency  */

/* general defs */
#define TRUE    1
#define FALSE   0

#define HI      1
#define LO      0

#define ON      1
#define OFF     0

/* Serial port configs*/
#define CONSOLE_PORT                0


/* configuration parameters */
#define CFG_DEFAULT_BOOT_FLAG       0x0000
#define CFG_ACTIVE_BOOT_FLAG        0xAAAA

#define CFG_INST_BAUD_4800        4800L
#define CFG_INST_BAUD_9600        9600L
#define CFG_INST_BAUD_14400       14400L
#define CFG_INST_BAUD_19200       19200L
#define CFG_INST_BAUD_38400       38400L
#define CFG_INST_BAUD_57600       57600L
#define CFG_INST_BAUD_115200      115200L
#define CFG_INST_BAUD_DEF         CFG_INST_BAUD_19200

#define CFG_INST_PAR_8NONE   0
#define CFG_INST_PAR_8EVEN   1
#define CFG_INST_PAR_8ODD    2
#define CFG_INST_PAR_9NONE   3
#define CFG_INST_PAR_DEF    CFG_INST_PAR_8NONE

#define CFG_INST_STOP_ONE   1
#define CFG_INST_STOP_TWO   2
#define CFG_INST_STOP_DEF   CFG_INST_STOP_ONE

/* debug bits */
#define CFG_DEBUG_BITS_DEF          0x00000000L
#define MBUS_DEBUG_BIT              0x00000001L
#define DIG_OUT_DEBUG_BIT           0x00000002L

#endif
