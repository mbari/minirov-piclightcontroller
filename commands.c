/****************************************************************************/
/* Copyright 2010 MBARI.                                                    */
/* MBARI Proprietary Information. All rights reserved.                      */
/****************************************************************************/

#include <stddef.h>
#include "actions.h"
#include "commands.h"

/****************************************************************************/
/*                            command structures                            */
/****************************************************************************/

const CmdStruct cmdsRoot[] =
{
    {"GET",                 actGetCmd               },
    {"SET",                 actSetCmd               },
    {"CLEAR|CLR",           actClrCmd               },
    {"HELP|?",              actHelpCmd              },
    {"RESET",               actResetCmd             },
    /* debug command(s) below */                    
    {"TEST1",               actTest1Cmd             },
    {"TEST2",               actTest2Cmd             },
    {"TEST3",               actTest3Cmd             },
    {"TEST4",               actTest4Cmd             },
    {"TEST5",               actTest5Cmd             },
    {"HELLO KITTY",         actHelloKittyCmd        },
    {"",                    NULL                    }
};                                                  
                                                    
const CmdStruct cmdsGet[] =                         
{                                                   
    {"STUB",                actGetStubCmd           },
//    {"PORTA",               actGetPortACmd          },
//    {"PORTB",               actGetPortBCmd          },
//    {"ENV",                 actGetEnvCmd            },
    {"DIN",                 actGetDINCmd            },
    {"AIN",                 actGetAINCmd            },
//    {"ID",                  actGetIdCmd             },
//    {"BAUD",                actGetBaudCmd           },
    {"INST0",                actGetInst0Cmd           },
    {"INST1",                actGetInst1Cmd           },
//    {"OUTDFLT",             actGetOutDfltCmd        },
//    {"CONFIG",              actGetConfigCmd         },
    {"",                    NULL                    }
};                                                 
                                                   
const CmdStruct cmdsSet[] =                        
{                                                  
    {"STUB",                actSetStubCmd           },
//    {"PORTA",               actSetPortACmd          },
//    {"PORTB",               actSetPortBCmd          },
//    {"ID",                  actSetIdCmd             },
//    {"BAUD",                actSetBaudCmd           },
//    {"OUTDFLT",             actSetOutDfltCmd        },
    {"LT0",                 actSetLT0Cmd            },
    {"LT1",                 actSetLT1Cmd            },
    {"LT2",                 actSetLT2Cmd            },
    {"LT3",                 actSetLT3Cmd            },
    {"LT4",                 actSetLT4Cmd            },
    {"LT5",                 actSetLT5Cmd            },
    {"IN0",                 actSetIN0Cmd            },
    {"IN1",                 actSetIN1Cmd            },
    {"DO0",                 actSetDO0Cmd            },
    {"DO1",                 actSetDO1Cmd            },
    {"GFL",                 actSetGFLCmd            },
    {"GFH",                 actSetGFHCmd            },
    {"VICOR",               actSetVicorCmd          },
    {"AO0",                 actSetAO0Cmd            },
    {"AO1",                 actSetAO1Cmd            },
    {"AO2",                 actSetAO2Cmd            },
    {"AO3",                 actSetAO3Cmd            },
    {"AO4",                 actSetAO4Cmd            },
    {"AO5",                 actSetAO5Cmd            },
    {"AO6",                 actSetAO6Cmd            },
    {"AO7",                 actSetAO7Cmd            },
    {"INST0",               actSetInst0Cfg          },
    {"INST1",               actSetInst1Cfg          },
    {"",                    NULL                    }
};

const CmdStruct cmdsClr[] =
{
    {"STUB",                actClrStubCmd           },
    {"",                    NULL                    }
};

