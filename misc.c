/****************************************************************************/
/* Copyright 2009 MBARI.                                                    */
/* MBARI Proprietary Information. All rights reserved.                      */
/****************************************************************************/

static int ucCriticalNesting = 0;

void EnterCriticalSection( void )
{
    /* if you're not in a critical section disable ints */
    if( ucCriticalNesting == 0 )
        __asm__ volatile("disi #0x3FFF");
    
    /* increment critical section nesting count */
    ++ucCriticalNesting;
}

void ExitCriticalSection( void )
{
    /* decrement critical section nesting count */
    --ucCriticalNesting;

    /* if you've nested all the way out 
    of critical sections enable ints */
    if( ucCriticalNesting == 0 )
        __asm__ volatile("disi #0x0000");
}

