/****************************************************************************/
/* Copyright 2010 MBARI.                                                    */
/* MBARI Proprietary Information. All rights reserved.                      */
/****************************************************************************/

/* general errors */
#define ERR_SUCCESS             0

#define ERR_FAILURE             1
#define ERR_EMPTY_LINE          2
#define ERR_NO_LINE             3
#define ERR_NO_COMMAND          4
#define ERR_NO_SUB_COMMAND      5


