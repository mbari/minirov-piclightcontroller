/****************************************************************************/
/* Copyright 2009 MBARI.                                                    */
/* MBARI Proprietary Information. All rights reserved.                      */
/****************************************************************************/
#ifndef BUFFER_H
#define BUFFER_H

typedef struct
{
    unsigned char* data;
    int head;
    int tail;
    int ptr_mask;
} ByteBuffer;

/* buffer access and status functions */
int bufPut(unsigned char b, ByteBuffer* buff);
int bufGet(unsigned char* b, ByteBuffer* buff);

void bufClear(ByteBuffer* buff);
int bufIsFull(ByteBuffer* buff);
int bufIsEmpty(ByteBuffer* buff);
int bufAvailable(ByteBuffer* buff);

#endif
