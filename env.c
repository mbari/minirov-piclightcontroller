/* 
 * File: env.c   
 * Author: E. Martin
 * Comments: environment functions file, which for this implementation
 * 		controls the leak sensor and the ground fault current 
 * 		sensor functionality. 
 *  
 * Revision history: Git via BitBucket 
 * 		https://bitbucket.org/mbari/minirov-piclightcontroller 
 * 
 * Copyright: 2016 MBARI, this is proprietary code, all rights reserved.
 *
 */

/* includes */
#include "modbus_registers.h"
