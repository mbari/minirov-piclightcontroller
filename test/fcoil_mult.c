/****************************************************************************/
/* Copyright 2014 MBARI.                                                    */
/* Monterey Bay Aquarium Research Institute Proprietary Information.        */
/* All rights reserved.                                                     */
/****************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

#include <modbus.h>

#define SLAVE_ID        0x01
#define DEFAULT_BAUD    19200L
#define TOTAL_COILS     16
#define MODBUS_DEBUG    TRUE

int main(int argc, char* argv[])
{
    int ret, i;
    int start;
    int ncoils;
    unsigned long coil_word, temp_coil_word;
    uint8_t coils[TOTAL_COILS];
    modbus_t *ctx;
    long baud;

    if ( argc < 5)
    {
        printf("FCOIL_MULT compiled with libmodbus version %s on %s at %s\n\n", 
               LIBMODBUS_VERSION_STRING, __DATE__, __TIME__);

        printf("Usage: fcoil_mult port start ncoils coil_word [baud]\n");
        printf("Default baud is %ld\n\n", DEFAULT_BAUD);
        printf("Example:\n");
        printf("./fcoil_mult com1  0 8 0xAA\n");
        return -1;
    }

    /* set coil start */
    start = atoi(argv[2])-1; //adjust for libmodbus address shift;;

    /* set coil address */
    ncoils = atoi(argv[3]);

    /* check the coil count */
    if ( ncoils > TOTAL_COILS )
    {
        printf("ERROR exceed max coils of %d\n", TOTAL_COILS);
        exit(1);
    }

    /* get the coil word */
    coil_word = strtoul(argv[4], NULL, 16);
    
    /* set the baud rate */
    baud = DEFAULT_BAUD;
    if ( argc == 6 )
    {
        baud = atol(argv[4]);
        switch ( baud )
        {
            case 9600:   break;
            case 19200:  break;
            case 38400:  break;
            case 57600:  break;
            case 115200: break;
            default: 
                printf("Invalid baud: %ld\n", baud);
                printf("Supported bauds: 9600, 19200, 38400, 57600, 115200\n");
                return -1;
        }
    }

    /* init the modbus interface */
    ctx = modbus_new_rtu(argv[1], baud, 'N', 8, 2);

    if (ctx == NULL)
    {
       fprintf(stderr, "Unable to allocate libmodbus context\n");
       return -1;
    }

    modbus_set_debug(ctx, MODBUS_DEBUG);
    modbus_set_error_recovery(ctx, MODBUS_ERROR_RECOVERY_LINK);
    modbus_set_slave(ctx, SLAVE_ID);

    if (modbus_connect(ctx) == -1)
    {
       fprintf(stderr, "ERROR Connection failed: %s\n",
               modbus_strerror(errno));
       modbus_free(ctx);
       return -1;
    }

    /* store the coil word in the array */
    temp_coil_word = coil_word;
    for (i = 0; i < ncoils; ++i)
    {
        coils[i] = temp_coil_word & 0x00000001;
        if (MODBUS_DEBUG)
           printf("COIL %02d = %d\n", (i + start), (int)coils[i]);
        
        /* shift to next coild word bit */
        temp_coil_word >>= 1;
    }
    
    /* write the state of the coils */
    if (MODBUS_DEBUG)
       printf("modbus_write_bits(SLAVE %d, START %d, NCOILS %d, COIL_WORD "\
              "0x%08lX): ", SLAVE_ID, start, ncoils, coil_word);
    
    ret = modbus_write_bits(ctx, start, ncoils, coils);

    /* check for errors */
    if (ret != ncoils) 
    {
       printf("FAILED\n");
       ret = -1;
    }
    else
    {
       printf("OK\n");
       ret = 0;
    }

    /* Close the connection */
    modbus_close(ctx);
    modbus_free(ctx);

    return ret;
}
