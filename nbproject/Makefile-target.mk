#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-target.mk)" "nbproject/Makefile-local-target.mk"
include nbproject/Makefile-local-target.mk
endif
endif

# Environment
MKDIR=gnumkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=target
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=elf
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/miniROV-picLightController.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/miniROV-picLightController.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

ifeq ($(COMPARE_BUILD), true)
COMPARISON_BUILD=-mafrlcsj
else
COMPARISON_BUILD=
endif

ifdef SUB_IMAGE_ADDRESS
SUB_IMAGE_ADDRESS_COMMAND=--image-address $(SUB_IMAGE_ADDRESS)
else
SUB_IMAGE_ADDRESS_COMMAND=
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=mcc_generated_files/spi1.c mcc_generated_files/pin_manager.c mcc_generated_files/interrupt_manager.c mcc_generated_files/mcc.c mcc_generated_files/adc1.c modbus/ascii/mbascii.c modbus/functions/mbfunccoils.c modbus/functions/mbfuncdiag.c modbus/functions/mbfuncdisc.c modbus/functions/mbfuncholding.c modbus/functions/mbfuncinput.c modbus/functions/mbfuncother.c modbus/functions/mbutils.c modbus/port/portevent.c modbus/port/portserial.c modbus/port/porttimer.c modbus/rtu/mbcrc.c modbus/rtu/mbrtu.c modbus/tcp/mbtcp.c modbus/mb.c main.c AD567x.c misc.c dig_out.c serial.c buffer.c analoginput.c dig_in.c env.c actions.c commands.c parser.c config.c flash_mem.c inst_port.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/mcc_generated_files/spi1.o ${OBJECTDIR}/mcc_generated_files/pin_manager.o ${OBJECTDIR}/mcc_generated_files/interrupt_manager.o ${OBJECTDIR}/mcc_generated_files/mcc.o ${OBJECTDIR}/mcc_generated_files/adc1.o ${OBJECTDIR}/modbus/ascii/mbascii.o ${OBJECTDIR}/modbus/functions/mbfunccoils.o ${OBJECTDIR}/modbus/functions/mbfuncdiag.o ${OBJECTDIR}/modbus/functions/mbfuncdisc.o ${OBJECTDIR}/modbus/functions/mbfuncholding.o ${OBJECTDIR}/modbus/functions/mbfuncinput.o ${OBJECTDIR}/modbus/functions/mbfuncother.o ${OBJECTDIR}/modbus/functions/mbutils.o ${OBJECTDIR}/modbus/port/portevent.o ${OBJECTDIR}/modbus/port/portserial.o ${OBJECTDIR}/modbus/port/porttimer.o ${OBJECTDIR}/modbus/rtu/mbcrc.o ${OBJECTDIR}/modbus/rtu/mbrtu.o ${OBJECTDIR}/modbus/tcp/mbtcp.o ${OBJECTDIR}/modbus/mb.o ${OBJECTDIR}/main.o ${OBJECTDIR}/AD567x.o ${OBJECTDIR}/misc.o ${OBJECTDIR}/dig_out.o ${OBJECTDIR}/serial.o ${OBJECTDIR}/buffer.o ${OBJECTDIR}/analoginput.o ${OBJECTDIR}/dig_in.o ${OBJECTDIR}/env.o ${OBJECTDIR}/actions.o ${OBJECTDIR}/commands.o ${OBJECTDIR}/parser.o ${OBJECTDIR}/config.o ${OBJECTDIR}/flash_mem.o ${OBJECTDIR}/inst_port.o
POSSIBLE_DEPFILES=${OBJECTDIR}/mcc_generated_files/spi1.o.d ${OBJECTDIR}/mcc_generated_files/pin_manager.o.d ${OBJECTDIR}/mcc_generated_files/interrupt_manager.o.d ${OBJECTDIR}/mcc_generated_files/mcc.o.d ${OBJECTDIR}/mcc_generated_files/adc1.o.d ${OBJECTDIR}/modbus/ascii/mbascii.o.d ${OBJECTDIR}/modbus/functions/mbfunccoils.o.d ${OBJECTDIR}/modbus/functions/mbfuncdiag.o.d ${OBJECTDIR}/modbus/functions/mbfuncdisc.o.d ${OBJECTDIR}/modbus/functions/mbfuncholding.o.d ${OBJECTDIR}/modbus/functions/mbfuncinput.o.d ${OBJECTDIR}/modbus/functions/mbfuncother.o.d ${OBJECTDIR}/modbus/functions/mbutils.o.d ${OBJECTDIR}/modbus/port/portevent.o.d ${OBJECTDIR}/modbus/port/portserial.o.d ${OBJECTDIR}/modbus/port/porttimer.o.d ${OBJECTDIR}/modbus/rtu/mbcrc.o.d ${OBJECTDIR}/modbus/rtu/mbrtu.o.d ${OBJECTDIR}/modbus/tcp/mbtcp.o.d ${OBJECTDIR}/modbus/mb.o.d ${OBJECTDIR}/main.o.d ${OBJECTDIR}/AD567x.o.d ${OBJECTDIR}/misc.o.d ${OBJECTDIR}/dig_out.o.d ${OBJECTDIR}/serial.o.d ${OBJECTDIR}/buffer.o.d ${OBJECTDIR}/analoginput.o.d ${OBJECTDIR}/dig_in.o.d ${OBJECTDIR}/env.o.d ${OBJECTDIR}/actions.o.d ${OBJECTDIR}/commands.o.d ${OBJECTDIR}/parser.o.d ${OBJECTDIR}/config.o.d ${OBJECTDIR}/flash_mem.o.d ${OBJECTDIR}/inst_port.o.d

# Object Files
OBJECTFILES=${OBJECTDIR}/mcc_generated_files/spi1.o ${OBJECTDIR}/mcc_generated_files/pin_manager.o ${OBJECTDIR}/mcc_generated_files/interrupt_manager.o ${OBJECTDIR}/mcc_generated_files/mcc.o ${OBJECTDIR}/mcc_generated_files/adc1.o ${OBJECTDIR}/modbus/ascii/mbascii.o ${OBJECTDIR}/modbus/functions/mbfunccoils.o ${OBJECTDIR}/modbus/functions/mbfuncdiag.o ${OBJECTDIR}/modbus/functions/mbfuncdisc.o ${OBJECTDIR}/modbus/functions/mbfuncholding.o ${OBJECTDIR}/modbus/functions/mbfuncinput.o ${OBJECTDIR}/modbus/functions/mbfuncother.o ${OBJECTDIR}/modbus/functions/mbutils.o ${OBJECTDIR}/modbus/port/portevent.o ${OBJECTDIR}/modbus/port/portserial.o ${OBJECTDIR}/modbus/port/porttimer.o ${OBJECTDIR}/modbus/rtu/mbcrc.o ${OBJECTDIR}/modbus/rtu/mbrtu.o ${OBJECTDIR}/modbus/tcp/mbtcp.o ${OBJECTDIR}/modbus/mb.o ${OBJECTDIR}/main.o ${OBJECTDIR}/AD567x.o ${OBJECTDIR}/misc.o ${OBJECTDIR}/dig_out.o ${OBJECTDIR}/serial.o ${OBJECTDIR}/buffer.o ${OBJECTDIR}/analoginput.o ${OBJECTDIR}/dig_in.o ${OBJECTDIR}/env.o ${OBJECTDIR}/actions.o ${OBJECTDIR}/commands.o ${OBJECTDIR}/parser.o ${OBJECTDIR}/config.o ${OBJECTDIR}/flash_mem.o ${OBJECTDIR}/inst_port.o

# Source Files
SOURCEFILES=mcc_generated_files/spi1.c mcc_generated_files/pin_manager.c mcc_generated_files/interrupt_manager.c mcc_generated_files/mcc.c mcc_generated_files/adc1.c modbus/ascii/mbascii.c modbus/functions/mbfunccoils.c modbus/functions/mbfuncdiag.c modbus/functions/mbfuncdisc.c modbus/functions/mbfuncholding.c modbus/functions/mbfuncinput.c modbus/functions/mbfuncother.c modbus/functions/mbutils.c modbus/port/portevent.c modbus/port/portserial.c modbus/port/porttimer.c modbus/rtu/mbcrc.c modbus/rtu/mbrtu.c modbus/tcp/mbtcp.c modbus/mb.c main.c AD567x.c misc.c dig_out.c serial.c buffer.c analoginput.c dig_in.c env.c actions.c commands.c parser.c config.c flash_mem.c inst_port.c


CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-target.mk dist/${CND_CONF}/${IMAGE_TYPE}/miniROV-picLightController.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=24FJ256GA106
MP_LINKER_FILE_OPTION=,--script=p24FJ256GA106.gld
# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/mcc_generated_files/spi1.o: mcc_generated_files/spi1.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/mcc_generated_files" 
	@${RM} ${OBJECTDIR}/mcc_generated_files/spi1.o.d 
	@${RM} ${OBJECTDIR}/mcc_generated_files/spi1.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  mcc_generated_files/spi1.c  -o ${OBJECTDIR}/mcc_generated_files/spi1.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/mcc_generated_files/spi1.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_REAL_ICE=1    -omf=elf -DXPRJ_target=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -I"modbus/ascii" -I"modbus/include" -I"modbus/port" -I"modbus/rtu" -I"./" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/mcc_generated_files/spi1.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/mcc_generated_files/pin_manager.o: mcc_generated_files/pin_manager.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/mcc_generated_files" 
	@${RM} ${OBJECTDIR}/mcc_generated_files/pin_manager.o.d 
	@${RM} ${OBJECTDIR}/mcc_generated_files/pin_manager.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  mcc_generated_files/pin_manager.c  -o ${OBJECTDIR}/mcc_generated_files/pin_manager.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/mcc_generated_files/pin_manager.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_REAL_ICE=1    -omf=elf -DXPRJ_target=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -I"modbus/ascii" -I"modbus/include" -I"modbus/port" -I"modbus/rtu" -I"./" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/mcc_generated_files/pin_manager.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/mcc_generated_files/interrupt_manager.o: mcc_generated_files/interrupt_manager.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/mcc_generated_files" 
	@${RM} ${OBJECTDIR}/mcc_generated_files/interrupt_manager.o.d 
	@${RM} ${OBJECTDIR}/mcc_generated_files/interrupt_manager.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  mcc_generated_files/interrupt_manager.c  -o ${OBJECTDIR}/mcc_generated_files/interrupt_manager.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/mcc_generated_files/interrupt_manager.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_REAL_ICE=1    -omf=elf -DXPRJ_target=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -I"modbus/ascii" -I"modbus/include" -I"modbus/port" -I"modbus/rtu" -I"./" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/mcc_generated_files/interrupt_manager.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/mcc_generated_files/mcc.o: mcc_generated_files/mcc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/mcc_generated_files" 
	@${RM} ${OBJECTDIR}/mcc_generated_files/mcc.o.d 
	@${RM} ${OBJECTDIR}/mcc_generated_files/mcc.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  mcc_generated_files/mcc.c  -o ${OBJECTDIR}/mcc_generated_files/mcc.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/mcc_generated_files/mcc.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_REAL_ICE=1    -omf=elf -DXPRJ_target=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -I"modbus/ascii" -I"modbus/include" -I"modbus/port" -I"modbus/rtu" -I"./" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/mcc_generated_files/mcc.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/mcc_generated_files/adc1.o: mcc_generated_files/adc1.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/mcc_generated_files" 
	@${RM} ${OBJECTDIR}/mcc_generated_files/adc1.o.d 
	@${RM} ${OBJECTDIR}/mcc_generated_files/adc1.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  mcc_generated_files/adc1.c  -o ${OBJECTDIR}/mcc_generated_files/adc1.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/mcc_generated_files/adc1.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_REAL_ICE=1    -omf=elf -DXPRJ_target=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -I"modbus/ascii" -I"modbus/include" -I"modbus/port" -I"modbus/rtu" -I"./" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/mcc_generated_files/adc1.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/modbus/ascii/mbascii.o: modbus/ascii/mbascii.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/modbus/ascii" 
	@${RM} ${OBJECTDIR}/modbus/ascii/mbascii.o.d 
	@${RM} ${OBJECTDIR}/modbus/ascii/mbascii.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  modbus/ascii/mbascii.c  -o ${OBJECTDIR}/modbus/ascii/mbascii.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/modbus/ascii/mbascii.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_REAL_ICE=1    -omf=elf -DXPRJ_target=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -I"modbus/ascii" -I"modbus/include" -I"modbus/port" -I"modbus/rtu" -I"./" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/modbus/ascii/mbascii.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/modbus/functions/mbfunccoils.o: modbus/functions/mbfunccoils.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/modbus/functions" 
	@${RM} ${OBJECTDIR}/modbus/functions/mbfunccoils.o.d 
	@${RM} ${OBJECTDIR}/modbus/functions/mbfunccoils.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  modbus/functions/mbfunccoils.c  -o ${OBJECTDIR}/modbus/functions/mbfunccoils.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/modbus/functions/mbfunccoils.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_REAL_ICE=1    -omf=elf -DXPRJ_target=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -I"modbus/ascii" -I"modbus/include" -I"modbus/port" -I"modbus/rtu" -I"./" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/modbus/functions/mbfunccoils.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/modbus/functions/mbfuncdiag.o: modbus/functions/mbfuncdiag.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/modbus/functions" 
	@${RM} ${OBJECTDIR}/modbus/functions/mbfuncdiag.o.d 
	@${RM} ${OBJECTDIR}/modbus/functions/mbfuncdiag.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  modbus/functions/mbfuncdiag.c  -o ${OBJECTDIR}/modbus/functions/mbfuncdiag.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/modbus/functions/mbfuncdiag.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_REAL_ICE=1    -omf=elf -DXPRJ_target=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -I"modbus/ascii" -I"modbus/include" -I"modbus/port" -I"modbus/rtu" -I"./" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/modbus/functions/mbfuncdiag.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/modbus/functions/mbfuncdisc.o: modbus/functions/mbfuncdisc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/modbus/functions" 
	@${RM} ${OBJECTDIR}/modbus/functions/mbfuncdisc.o.d 
	@${RM} ${OBJECTDIR}/modbus/functions/mbfuncdisc.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  modbus/functions/mbfuncdisc.c  -o ${OBJECTDIR}/modbus/functions/mbfuncdisc.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/modbus/functions/mbfuncdisc.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_REAL_ICE=1    -omf=elf -DXPRJ_target=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -I"modbus/ascii" -I"modbus/include" -I"modbus/port" -I"modbus/rtu" -I"./" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/modbus/functions/mbfuncdisc.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/modbus/functions/mbfuncholding.o: modbus/functions/mbfuncholding.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/modbus/functions" 
	@${RM} ${OBJECTDIR}/modbus/functions/mbfuncholding.o.d 
	@${RM} ${OBJECTDIR}/modbus/functions/mbfuncholding.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  modbus/functions/mbfuncholding.c  -o ${OBJECTDIR}/modbus/functions/mbfuncholding.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/modbus/functions/mbfuncholding.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_REAL_ICE=1    -omf=elf -DXPRJ_target=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -I"modbus/ascii" -I"modbus/include" -I"modbus/port" -I"modbus/rtu" -I"./" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/modbus/functions/mbfuncholding.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/modbus/functions/mbfuncinput.o: modbus/functions/mbfuncinput.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/modbus/functions" 
	@${RM} ${OBJECTDIR}/modbus/functions/mbfuncinput.o.d 
	@${RM} ${OBJECTDIR}/modbus/functions/mbfuncinput.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  modbus/functions/mbfuncinput.c  -o ${OBJECTDIR}/modbus/functions/mbfuncinput.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/modbus/functions/mbfuncinput.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_REAL_ICE=1    -omf=elf -DXPRJ_target=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -I"modbus/ascii" -I"modbus/include" -I"modbus/port" -I"modbus/rtu" -I"./" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/modbus/functions/mbfuncinput.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/modbus/functions/mbfuncother.o: modbus/functions/mbfuncother.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/modbus/functions" 
	@${RM} ${OBJECTDIR}/modbus/functions/mbfuncother.o.d 
	@${RM} ${OBJECTDIR}/modbus/functions/mbfuncother.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  modbus/functions/mbfuncother.c  -o ${OBJECTDIR}/modbus/functions/mbfuncother.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/modbus/functions/mbfuncother.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_REAL_ICE=1    -omf=elf -DXPRJ_target=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -I"modbus/ascii" -I"modbus/include" -I"modbus/port" -I"modbus/rtu" -I"./" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/modbus/functions/mbfuncother.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/modbus/functions/mbutils.o: modbus/functions/mbutils.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/modbus/functions" 
	@${RM} ${OBJECTDIR}/modbus/functions/mbutils.o.d 
	@${RM} ${OBJECTDIR}/modbus/functions/mbutils.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  modbus/functions/mbutils.c  -o ${OBJECTDIR}/modbus/functions/mbutils.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/modbus/functions/mbutils.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_REAL_ICE=1    -omf=elf -DXPRJ_target=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -I"modbus/ascii" -I"modbus/include" -I"modbus/port" -I"modbus/rtu" -I"./" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/modbus/functions/mbutils.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/modbus/port/portevent.o: modbus/port/portevent.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/modbus/port" 
	@${RM} ${OBJECTDIR}/modbus/port/portevent.o.d 
	@${RM} ${OBJECTDIR}/modbus/port/portevent.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  modbus/port/portevent.c  -o ${OBJECTDIR}/modbus/port/portevent.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/modbus/port/portevent.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_REAL_ICE=1    -omf=elf -DXPRJ_target=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -I"modbus/ascii" -I"modbus/include" -I"modbus/port" -I"modbus/rtu" -I"./" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/modbus/port/portevent.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/modbus/port/portserial.o: modbus/port/portserial.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/modbus/port" 
	@${RM} ${OBJECTDIR}/modbus/port/portserial.o.d 
	@${RM} ${OBJECTDIR}/modbus/port/portserial.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  modbus/port/portserial.c  -o ${OBJECTDIR}/modbus/port/portserial.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/modbus/port/portserial.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_REAL_ICE=1    -omf=elf -DXPRJ_target=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -I"modbus/ascii" -I"modbus/include" -I"modbus/port" -I"modbus/rtu" -I"./" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/modbus/port/portserial.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/modbus/port/porttimer.o: modbus/port/porttimer.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/modbus/port" 
	@${RM} ${OBJECTDIR}/modbus/port/porttimer.o.d 
	@${RM} ${OBJECTDIR}/modbus/port/porttimer.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  modbus/port/porttimer.c  -o ${OBJECTDIR}/modbus/port/porttimer.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/modbus/port/porttimer.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_REAL_ICE=1    -omf=elf -DXPRJ_target=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -I"modbus/ascii" -I"modbus/include" -I"modbus/port" -I"modbus/rtu" -I"./" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/modbus/port/porttimer.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/modbus/rtu/mbcrc.o: modbus/rtu/mbcrc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/modbus/rtu" 
	@${RM} ${OBJECTDIR}/modbus/rtu/mbcrc.o.d 
	@${RM} ${OBJECTDIR}/modbus/rtu/mbcrc.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  modbus/rtu/mbcrc.c  -o ${OBJECTDIR}/modbus/rtu/mbcrc.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/modbus/rtu/mbcrc.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_REAL_ICE=1    -omf=elf -DXPRJ_target=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -I"modbus/ascii" -I"modbus/include" -I"modbus/port" -I"modbus/rtu" -I"./" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/modbus/rtu/mbcrc.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/modbus/rtu/mbrtu.o: modbus/rtu/mbrtu.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/modbus/rtu" 
	@${RM} ${OBJECTDIR}/modbus/rtu/mbrtu.o.d 
	@${RM} ${OBJECTDIR}/modbus/rtu/mbrtu.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  modbus/rtu/mbrtu.c  -o ${OBJECTDIR}/modbus/rtu/mbrtu.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/modbus/rtu/mbrtu.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_REAL_ICE=1    -omf=elf -DXPRJ_target=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -I"modbus/ascii" -I"modbus/include" -I"modbus/port" -I"modbus/rtu" -I"./" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/modbus/rtu/mbrtu.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/modbus/tcp/mbtcp.o: modbus/tcp/mbtcp.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/modbus/tcp" 
	@${RM} ${OBJECTDIR}/modbus/tcp/mbtcp.o.d 
	@${RM} ${OBJECTDIR}/modbus/tcp/mbtcp.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  modbus/tcp/mbtcp.c  -o ${OBJECTDIR}/modbus/tcp/mbtcp.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/modbus/tcp/mbtcp.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_REAL_ICE=1    -omf=elf -DXPRJ_target=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -I"modbus/ascii" -I"modbus/include" -I"modbus/port" -I"modbus/rtu" -I"./" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/modbus/tcp/mbtcp.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/modbus/mb.o: modbus/mb.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/modbus" 
	@${RM} ${OBJECTDIR}/modbus/mb.o.d 
	@${RM} ${OBJECTDIR}/modbus/mb.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  modbus/mb.c  -o ${OBJECTDIR}/modbus/mb.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/modbus/mb.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_REAL_ICE=1    -omf=elf -DXPRJ_target=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -I"modbus/ascii" -I"modbus/include" -I"modbus/port" -I"modbus/rtu" -I"./" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/modbus/mb.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/main.o: main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/main.o.d 
	@${RM} ${OBJECTDIR}/main.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  main.c  -o ${OBJECTDIR}/main.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/main.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_REAL_ICE=1    -omf=elf -DXPRJ_target=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -I"modbus/ascii" -I"modbus/include" -I"modbus/port" -I"modbus/rtu" -I"./" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/main.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/AD567x.o: AD567x.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/AD567x.o.d 
	@${RM} ${OBJECTDIR}/AD567x.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  AD567x.c  -o ${OBJECTDIR}/AD567x.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/AD567x.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_REAL_ICE=1    -omf=elf -DXPRJ_target=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -I"modbus/ascii" -I"modbus/include" -I"modbus/port" -I"modbus/rtu" -I"./" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/AD567x.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/misc.o: misc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/misc.o.d 
	@${RM} ${OBJECTDIR}/misc.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  misc.c  -o ${OBJECTDIR}/misc.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/misc.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_REAL_ICE=1    -omf=elf -DXPRJ_target=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -I"modbus/ascii" -I"modbus/include" -I"modbus/port" -I"modbus/rtu" -I"./" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/misc.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/dig_out.o: dig_out.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/dig_out.o.d 
	@${RM} ${OBJECTDIR}/dig_out.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  dig_out.c  -o ${OBJECTDIR}/dig_out.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/dig_out.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_REAL_ICE=1    -omf=elf -DXPRJ_target=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -I"modbus/ascii" -I"modbus/include" -I"modbus/port" -I"modbus/rtu" -I"./" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/dig_out.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/serial.o: serial.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/serial.o.d 
	@${RM} ${OBJECTDIR}/serial.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  serial.c  -o ${OBJECTDIR}/serial.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/serial.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_REAL_ICE=1    -omf=elf -DXPRJ_target=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -I"modbus/ascii" -I"modbus/include" -I"modbus/port" -I"modbus/rtu" -I"./" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/serial.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/buffer.o: buffer.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/buffer.o.d 
	@${RM} ${OBJECTDIR}/buffer.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  buffer.c  -o ${OBJECTDIR}/buffer.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/buffer.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_REAL_ICE=1    -omf=elf -DXPRJ_target=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -I"modbus/ascii" -I"modbus/include" -I"modbus/port" -I"modbus/rtu" -I"./" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/buffer.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/analoginput.o: analoginput.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/analoginput.o.d 
	@${RM} ${OBJECTDIR}/analoginput.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  analoginput.c  -o ${OBJECTDIR}/analoginput.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/analoginput.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_REAL_ICE=1    -omf=elf -DXPRJ_target=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -I"modbus/ascii" -I"modbus/include" -I"modbus/port" -I"modbus/rtu" -I"./" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/analoginput.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/dig_in.o: dig_in.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/dig_in.o.d 
	@${RM} ${OBJECTDIR}/dig_in.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  dig_in.c  -o ${OBJECTDIR}/dig_in.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/dig_in.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_REAL_ICE=1    -omf=elf -DXPRJ_target=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -I"modbus/ascii" -I"modbus/include" -I"modbus/port" -I"modbus/rtu" -I"./" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/dig_in.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/env.o: env.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/env.o.d 
	@${RM} ${OBJECTDIR}/env.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  env.c  -o ${OBJECTDIR}/env.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/env.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_REAL_ICE=1    -omf=elf -DXPRJ_target=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -I"modbus/ascii" -I"modbus/include" -I"modbus/port" -I"modbus/rtu" -I"./" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/env.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/actions.o: actions.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/actions.o.d 
	@${RM} ${OBJECTDIR}/actions.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  actions.c  -o ${OBJECTDIR}/actions.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/actions.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_REAL_ICE=1    -omf=elf -DXPRJ_target=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -I"modbus/ascii" -I"modbus/include" -I"modbus/port" -I"modbus/rtu" -I"./" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/actions.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/commands.o: commands.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/commands.o.d 
	@${RM} ${OBJECTDIR}/commands.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  commands.c  -o ${OBJECTDIR}/commands.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/commands.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_REAL_ICE=1    -omf=elf -DXPRJ_target=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -I"modbus/ascii" -I"modbus/include" -I"modbus/port" -I"modbus/rtu" -I"./" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/commands.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/parser.o: parser.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/parser.o.d 
	@${RM} ${OBJECTDIR}/parser.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  parser.c  -o ${OBJECTDIR}/parser.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/parser.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_REAL_ICE=1    -omf=elf -DXPRJ_target=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -I"modbus/ascii" -I"modbus/include" -I"modbus/port" -I"modbus/rtu" -I"./" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/parser.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/config.o: config.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/config.o.d 
	@${RM} ${OBJECTDIR}/config.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  config.c  -o ${OBJECTDIR}/config.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/config.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_REAL_ICE=1    -omf=elf -DXPRJ_target=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -I"modbus/ascii" -I"modbus/include" -I"modbus/port" -I"modbus/rtu" -I"./" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/config.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/flash_mem.o: flash_mem.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/flash_mem.o.d 
	@${RM} ${OBJECTDIR}/flash_mem.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  flash_mem.c  -o ${OBJECTDIR}/flash_mem.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/flash_mem.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_REAL_ICE=1    -omf=elf -DXPRJ_target=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -I"modbus/ascii" -I"modbus/include" -I"modbus/port" -I"modbus/rtu" -I"./" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/flash_mem.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/inst_port.o: inst_port.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/inst_port.o.d 
	@${RM} ${OBJECTDIR}/inst_port.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  inst_port.c  -o ${OBJECTDIR}/inst_port.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/inst_port.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_REAL_ICE=1    -omf=elf -DXPRJ_target=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -I"modbus/ascii" -I"modbus/include" -I"modbus/port" -I"modbus/rtu" -I"./" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/inst_port.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
else
${OBJECTDIR}/mcc_generated_files/spi1.o: mcc_generated_files/spi1.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/mcc_generated_files" 
	@${RM} ${OBJECTDIR}/mcc_generated_files/spi1.o.d 
	@${RM} ${OBJECTDIR}/mcc_generated_files/spi1.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  mcc_generated_files/spi1.c  -o ${OBJECTDIR}/mcc_generated_files/spi1.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/mcc_generated_files/spi1.o.d"        -g -omf=elf -DXPRJ_target=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -I"modbus/ascii" -I"modbus/include" -I"modbus/port" -I"modbus/rtu" -I"./" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/mcc_generated_files/spi1.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/mcc_generated_files/pin_manager.o: mcc_generated_files/pin_manager.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/mcc_generated_files" 
	@${RM} ${OBJECTDIR}/mcc_generated_files/pin_manager.o.d 
	@${RM} ${OBJECTDIR}/mcc_generated_files/pin_manager.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  mcc_generated_files/pin_manager.c  -o ${OBJECTDIR}/mcc_generated_files/pin_manager.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/mcc_generated_files/pin_manager.o.d"        -g -omf=elf -DXPRJ_target=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -I"modbus/ascii" -I"modbus/include" -I"modbus/port" -I"modbus/rtu" -I"./" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/mcc_generated_files/pin_manager.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/mcc_generated_files/interrupt_manager.o: mcc_generated_files/interrupt_manager.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/mcc_generated_files" 
	@${RM} ${OBJECTDIR}/mcc_generated_files/interrupt_manager.o.d 
	@${RM} ${OBJECTDIR}/mcc_generated_files/interrupt_manager.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  mcc_generated_files/interrupt_manager.c  -o ${OBJECTDIR}/mcc_generated_files/interrupt_manager.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/mcc_generated_files/interrupt_manager.o.d"        -g -omf=elf -DXPRJ_target=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -I"modbus/ascii" -I"modbus/include" -I"modbus/port" -I"modbus/rtu" -I"./" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/mcc_generated_files/interrupt_manager.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/mcc_generated_files/mcc.o: mcc_generated_files/mcc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/mcc_generated_files" 
	@${RM} ${OBJECTDIR}/mcc_generated_files/mcc.o.d 
	@${RM} ${OBJECTDIR}/mcc_generated_files/mcc.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  mcc_generated_files/mcc.c  -o ${OBJECTDIR}/mcc_generated_files/mcc.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/mcc_generated_files/mcc.o.d"        -g -omf=elf -DXPRJ_target=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -I"modbus/ascii" -I"modbus/include" -I"modbus/port" -I"modbus/rtu" -I"./" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/mcc_generated_files/mcc.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/mcc_generated_files/adc1.o: mcc_generated_files/adc1.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/mcc_generated_files" 
	@${RM} ${OBJECTDIR}/mcc_generated_files/adc1.o.d 
	@${RM} ${OBJECTDIR}/mcc_generated_files/adc1.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  mcc_generated_files/adc1.c  -o ${OBJECTDIR}/mcc_generated_files/adc1.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/mcc_generated_files/adc1.o.d"        -g -omf=elf -DXPRJ_target=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -I"modbus/ascii" -I"modbus/include" -I"modbus/port" -I"modbus/rtu" -I"./" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/mcc_generated_files/adc1.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/modbus/ascii/mbascii.o: modbus/ascii/mbascii.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/modbus/ascii" 
	@${RM} ${OBJECTDIR}/modbus/ascii/mbascii.o.d 
	@${RM} ${OBJECTDIR}/modbus/ascii/mbascii.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  modbus/ascii/mbascii.c  -o ${OBJECTDIR}/modbus/ascii/mbascii.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/modbus/ascii/mbascii.o.d"        -g -omf=elf -DXPRJ_target=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -I"modbus/ascii" -I"modbus/include" -I"modbus/port" -I"modbus/rtu" -I"./" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/modbus/ascii/mbascii.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/modbus/functions/mbfunccoils.o: modbus/functions/mbfunccoils.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/modbus/functions" 
	@${RM} ${OBJECTDIR}/modbus/functions/mbfunccoils.o.d 
	@${RM} ${OBJECTDIR}/modbus/functions/mbfunccoils.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  modbus/functions/mbfunccoils.c  -o ${OBJECTDIR}/modbus/functions/mbfunccoils.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/modbus/functions/mbfunccoils.o.d"        -g -omf=elf -DXPRJ_target=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -I"modbus/ascii" -I"modbus/include" -I"modbus/port" -I"modbus/rtu" -I"./" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/modbus/functions/mbfunccoils.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/modbus/functions/mbfuncdiag.o: modbus/functions/mbfuncdiag.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/modbus/functions" 
	@${RM} ${OBJECTDIR}/modbus/functions/mbfuncdiag.o.d 
	@${RM} ${OBJECTDIR}/modbus/functions/mbfuncdiag.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  modbus/functions/mbfuncdiag.c  -o ${OBJECTDIR}/modbus/functions/mbfuncdiag.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/modbus/functions/mbfuncdiag.o.d"        -g -omf=elf -DXPRJ_target=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -I"modbus/ascii" -I"modbus/include" -I"modbus/port" -I"modbus/rtu" -I"./" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/modbus/functions/mbfuncdiag.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/modbus/functions/mbfuncdisc.o: modbus/functions/mbfuncdisc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/modbus/functions" 
	@${RM} ${OBJECTDIR}/modbus/functions/mbfuncdisc.o.d 
	@${RM} ${OBJECTDIR}/modbus/functions/mbfuncdisc.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  modbus/functions/mbfuncdisc.c  -o ${OBJECTDIR}/modbus/functions/mbfuncdisc.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/modbus/functions/mbfuncdisc.o.d"        -g -omf=elf -DXPRJ_target=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -I"modbus/ascii" -I"modbus/include" -I"modbus/port" -I"modbus/rtu" -I"./" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/modbus/functions/mbfuncdisc.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/modbus/functions/mbfuncholding.o: modbus/functions/mbfuncholding.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/modbus/functions" 
	@${RM} ${OBJECTDIR}/modbus/functions/mbfuncholding.o.d 
	@${RM} ${OBJECTDIR}/modbus/functions/mbfuncholding.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  modbus/functions/mbfuncholding.c  -o ${OBJECTDIR}/modbus/functions/mbfuncholding.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/modbus/functions/mbfuncholding.o.d"        -g -omf=elf -DXPRJ_target=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -I"modbus/ascii" -I"modbus/include" -I"modbus/port" -I"modbus/rtu" -I"./" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/modbus/functions/mbfuncholding.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/modbus/functions/mbfuncinput.o: modbus/functions/mbfuncinput.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/modbus/functions" 
	@${RM} ${OBJECTDIR}/modbus/functions/mbfuncinput.o.d 
	@${RM} ${OBJECTDIR}/modbus/functions/mbfuncinput.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  modbus/functions/mbfuncinput.c  -o ${OBJECTDIR}/modbus/functions/mbfuncinput.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/modbus/functions/mbfuncinput.o.d"        -g -omf=elf -DXPRJ_target=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -I"modbus/ascii" -I"modbus/include" -I"modbus/port" -I"modbus/rtu" -I"./" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/modbus/functions/mbfuncinput.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/modbus/functions/mbfuncother.o: modbus/functions/mbfuncother.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/modbus/functions" 
	@${RM} ${OBJECTDIR}/modbus/functions/mbfuncother.o.d 
	@${RM} ${OBJECTDIR}/modbus/functions/mbfuncother.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  modbus/functions/mbfuncother.c  -o ${OBJECTDIR}/modbus/functions/mbfuncother.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/modbus/functions/mbfuncother.o.d"        -g -omf=elf -DXPRJ_target=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -I"modbus/ascii" -I"modbus/include" -I"modbus/port" -I"modbus/rtu" -I"./" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/modbus/functions/mbfuncother.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/modbus/functions/mbutils.o: modbus/functions/mbutils.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/modbus/functions" 
	@${RM} ${OBJECTDIR}/modbus/functions/mbutils.o.d 
	@${RM} ${OBJECTDIR}/modbus/functions/mbutils.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  modbus/functions/mbutils.c  -o ${OBJECTDIR}/modbus/functions/mbutils.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/modbus/functions/mbutils.o.d"        -g -omf=elf -DXPRJ_target=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -I"modbus/ascii" -I"modbus/include" -I"modbus/port" -I"modbus/rtu" -I"./" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/modbus/functions/mbutils.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/modbus/port/portevent.o: modbus/port/portevent.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/modbus/port" 
	@${RM} ${OBJECTDIR}/modbus/port/portevent.o.d 
	@${RM} ${OBJECTDIR}/modbus/port/portevent.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  modbus/port/portevent.c  -o ${OBJECTDIR}/modbus/port/portevent.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/modbus/port/portevent.o.d"        -g -omf=elf -DXPRJ_target=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -I"modbus/ascii" -I"modbus/include" -I"modbus/port" -I"modbus/rtu" -I"./" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/modbus/port/portevent.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/modbus/port/portserial.o: modbus/port/portserial.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/modbus/port" 
	@${RM} ${OBJECTDIR}/modbus/port/portserial.o.d 
	@${RM} ${OBJECTDIR}/modbus/port/portserial.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  modbus/port/portserial.c  -o ${OBJECTDIR}/modbus/port/portserial.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/modbus/port/portserial.o.d"        -g -omf=elf -DXPRJ_target=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -I"modbus/ascii" -I"modbus/include" -I"modbus/port" -I"modbus/rtu" -I"./" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/modbus/port/portserial.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/modbus/port/porttimer.o: modbus/port/porttimer.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/modbus/port" 
	@${RM} ${OBJECTDIR}/modbus/port/porttimer.o.d 
	@${RM} ${OBJECTDIR}/modbus/port/porttimer.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  modbus/port/porttimer.c  -o ${OBJECTDIR}/modbus/port/porttimer.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/modbus/port/porttimer.o.d"        -g -omf=elf -DXPRJ_target=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -I"modbus/ascii" -I"modbus/include" -I"modbus/port" -I"modbus/rtu" -I"./" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/modbus/port/porttimer.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/modbus/rtu/mbcrc.o: modbus/rtu/mbcrc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/modbus/rtu" 
	@${RM} ${OBJECTDIR}/modbus/rtu/mbcrc.o.d 
	@${RM} ${OBJECTDIR}/modbus/rtu/mbcrc.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  modbus/rtu/mbcrc.c  -o ${OBJECTDIR}/modbus/rtu/mbcrc.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/modbus/rtu/mbcrc.o.d"        -g -omf=elf -DXPRJ_target=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -I"modbus/ascii" -I"modbus/include" -I"modbus/port" -I"modbus/rtu" -I"./" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/modbus/rtu/mbcrc.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/modbus/rtu/mbrtu.o: modbus/rtu/mbrtu.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/modbus/rtu" 
	@${RM} ${OBJECTDIR}/modbus/rtu/mbrtu.o.d 
	@${RM} ${OBJECTDIR}/modbus/rtu/mbrtu.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  modbus/rtu/mbrtu.c  -o ${OBJECTDIR}/modbus/rtu/mbrtu.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/modbus/rtu/mbrtu.o.d"        -g -omf=elf -DXPRJ_target=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -I"modbus/ascii" -I"modbus/include" -I"modbus/port" -I"modbus/rtu" -I"./" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/modbus/rtu/mbrtu.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/modbus/tcp/mbtcp.o: modbus/tcp/mbtcp.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/modbus/tcp" 
	@${RM} ${OBJECTDIR}/modbus/tcp/mbtcp.o.d 
	@${RM} ${OBJECTDIR}/modbus/tcp/mbtcp.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  modbus/tcp/mbtcp.c  -o ${OBJECTDIR}/modbus/tcp/mbtcp.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/modbus/tcp/mbtcp.o.d"        -g -omf=elf -DXPRJ_target=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -I"modbus/ascii" -I"modbus/include" -I"modbus/port" -I"modbus/rtu" -I"./" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/modbus/tcp/mbtcp.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/modbus/mb.o: modbus/mb.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/modbus" 
	@${RM} ${OBJECTDIR}/modbus/mb.o.d 
	@${RM} ${OBJECTDIR}/modbus/mb.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  modbus/mb.c  -o ${OBJECTDIR}/modbus/mb.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/modbus/mb.o.d"        -g -omf=elf -DXPRJ_target=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -I"modbus/ascii" -I"modbus/include" -I"modbus/port" -I"modbus/rtu" -I"./" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/modbus/mb.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/main.o: main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/main.o.d 
	@${RM} ${OBJECTDIR}/main.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  main.c  -o ${OBJECTDIR}/main.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/main.o.d"        -g -omf=elf -DXPRJ_target=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -I"modbus/ascii" -I"modbus/include" -I"modbus/port" -I"modbus/rtu" -I"./" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/main.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/AD567x.o: AD567x.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/AD567x.o.d 
	@${RM} ${OBJECTDIR}/AD567x.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  AD567x.c  -o ${OBJECTDIR}/AD567x.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/AD567x.o.d"        -g -omf=elf -DXPRJ_target=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -I"modbus/ascii" -I"modbus/include" -I"modbus/port" -I"modbus/rtu" -I"./" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/AD567x.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/misc.o: misc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/misc.o.d 
	@${RM} ${OBJECTDIR}/misc.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  misc.c  -o ${OBJECTDIR}/misc.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/misc.o.d"        -g -omf=elf -DXPRJ_target=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -I"modbus/ascii" -I"modbus/include" -I"modbus/port" -I"modbus/rtu" -I"./" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/misc.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/dig_out.o: dig_out.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/dig_out.o.d 
	@${RM} ${OBJECTDIR}/dig_out.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  dig_out.c  -o ${OBJECTDIR}/dig_out.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/dig_out.o.d"        -g -omf=elf -DXPRJ_target=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -I"modbus/ascii" -I"modbus/include" -I"modbus/port" -I"modbus/rtu" -I"./" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/dig_out.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/serial.o: serial.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/serial.o.d 
	@${RM} ${OBJECTDIR}/serial.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  serial.c  -o ${OBJECTDIR}/serial.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/serial.o.d"        -g -omf=elf -DXPRJ_target=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -I"modbus/ascii" -I"modbus/include" -I"modbus/port" -I"modbus/rtu" -I"./" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/serial.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/buffer.o: buffer.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/buffer.o.d 
	@${RM} ${OBJECTDIR}/buffer.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  buffer.c  -o ${OBJECTDIR}/buffer.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/buffer.o.d"        -g -omf=elf -DXPRJ_target=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -I"modbus/ascii" -I"modbus/include" -I"modbus/port" -I"modbus/rtu" -I"./" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/buffer.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/analoginput.o: analoginput.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/analoginput.o.d 
	@${RM} ${OBJECTDIR}/analoginput.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  analoginput.c  -o ${OBJECTDIR}/analoginput.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/analoginput.o.d"        -g -omf=elf -DXPRJ_target=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -I"modbus/ascii" -I"modbus/include" -I"modbus/port" -I"modbus/rtu" -I"./" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/analoginput.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/dig_in.o: dig_in.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/dig_in.o.d 
	@${RM} ${OBJECTDIR}/dig_in.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  dig_in.c  -o ${OBJECTDIR}/dig_in.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/dig_in.o.d"        -g -omf=elf -DXPRJ_target=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -I"modbus/ascii" -I"modbus/include" -I"modbus/port" -I"modbus/rtu" -I"./" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/dig_in.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/env.o: env.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/env.o.d 
	@${RM} ${OBJECTDIR}/env.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  env.c  -o ${OBJECTDIR}/env.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/env.o.d"        -g -omf=elf -DXPRJ_target=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -I"modbus/ascii" -I"modbus/include" -I"modbus/port" -I"modbus/rtu" -I"./" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/env.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/actions.o: actions.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/actions.o.d 
	@${RM} ${OBJECTDIR}/actions.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  actions.c  -o ${OBJECTDIR}/actions.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/actions.o.d"        -g -omf=elf -DXPRJ_target=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -I"modbus/ascii" -I"modbus/include" -I"modbus/port" -I"modbus/rtu" -I"./" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/actions.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/commands.o: commands.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/commands.o.d 
	@${RM} ${OBJECTDIR}/commands.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  commands.c  -o ${OBJECTDIR}/commands.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/commands.o.d"        -g -omf=elf -DXPRJ_target=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -I"modbus/ascii" -I"modbus/include" -I"modbus/port" -I"modbus/rtu" -I"./" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/commands.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/parser.o: parser.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/parser.o.d 
	@${RM} ${OBJECTDIR}/parser.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  parser.c  -o ${OBJECTDIR}/parser.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/parser.o.d"        -g -omf=elf -DXPRJ_target=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -I"modbus/ascii" -I"modbus/include" -I"modbus/port" -I"modbus/rtu" -I"./" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/parser.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/config.o: config.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/config.o.d 
	@${RM} ${OBJECTDIR}/config.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  config.c  -o ${OBJECTDIR}/config.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/config.o.d"        -g -omf=elf -DXPRJ_target=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -I"modbus/ascii" -I"modbus/include" -I"modbus/port" -I"modbus/rtu" -I"./" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/config.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/flash_mem.o: flash_mem.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/flash_mem.o.d 
	@${RM} ${OBJECTDIR}/flash_mem.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  flash_mem.c  -o ${OBJECTDIR}/flash_mem.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/flash_mem.o.d"        -g -omf=elf -DXPRJ_target=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -I"modbus/ascii" -I"modbus/include" -I"modbus/port" -I"modbus/rtu" -I"./" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/flash_mem.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/inst_port.o: inst_port.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/inst_port.o.d 
	@${RM} ${OBJECTDIR}/inst_port.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  inst_port.c  -o ${OBJECTDIR}/inst_port.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/inst_port.o.d"        -g -omf=elf -DXPRJ_target=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -I"modbus/ascii" -I"modbus/include" -I"modbus/port" -I"modbus/rtu" -I"./" -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/inst_port.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assemblePreproc
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/miniROV-picLightController.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk    
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -o dist/${CND_CONF}/${IMAGE_TYPE}/miniROV-picLightController.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}      -mcpu=$(MP_PROCESSOR_OPTION)        -D__DEBUG=__DEBUG -D__MPLAB_DEBUGGER_REAL_ICE=1  -omf=elf -DXPRJ_target=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)   -mreserve=data@0x800:0x81F -mreserve=data@0x820:0x821 -mreserve=data@0x822:0x823 -mreserve=data@0x824:0x825 -mreserve=data@0x826:0x84F   -Wl,,,--defsym=__MPLAB_BUILD=1,--defsym=__MPLAB_DEBUG=1,--defsym=__DEBUG=1,-D__DEBUG=__DEBUG,--defsym=__MPLAB_DEBUGGER_REAL_ICE=1,$(MP_LINKER_FILE_OPTION),--stack=16,--check-sections,--data-init,--pack-data,--handles,--isr,--no-gc-sections,--fill-upper=0,--stackguard=16,--no-force-link,--smart-io,-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map",--report-mem,--memorysummary,dist/${CND_CONF}/${IMAGE_TYPE}/memoryfile.xml$(MP_EXTRA_LD_POST) 
	
else
dist/${CND_CONF}/${IMAGE_TYPE}/miniROV-picLightController.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk   
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -o dist/${CND_CONF}/${IMAGE_TYPE}/miniROV-picLightController.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}      -mcpu=$(MP_PROCESSOR_OPTION)        -omf=elf -DXPRJ_target=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -Wl,,,--defsym=__MPLAB_BUILD=1,$(MP_LINKER_FILE_OPTION),--stack=16,--check-sections,--data-init,--pack-data,--handles,--isr,--no-gc-sections,--fill-upper=0,--stackguard=16,--no-force-link,--smart-io,-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map",--report-mem,--memorysummary,dist/${CND_CONF}/${IMAGE_TYPE}/memoryfile.xml$(MP_EXTRA_LD_POST) 
	${MP_CC_DIR}\\xc16-bin2hex dist/${CND_CONF}/${IMAGE_TYPE}/miniROV-picLightController.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} -a  -omf=elf  
	
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/target
	${RM} -r dist/target

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
