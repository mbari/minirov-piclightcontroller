#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
# explorer16 configuration
CND_ARTIFACT_DIR_explorer16=dist/explorer16/production
CND_ARTIFACT_NAME_explorer16=miniROV-picLightController.X.production.hex
CND_ARTIFACT_PATH_explorer16=dist/explorer16/production/miniROV-picLightController.X.production.hex
CND_PACKAGE_DIR_explorer16=${CND_DISTDIR}/explorer16/package
CND_PACKAGE_NAME_explorer16=minirov-piclightcontroller.x.tar
CND_PACKAGE_PATH_explorer16=${CND_DISTDIR}/explorer16/package/minirov-piclightcontroller.x.tar
# target configuration
CND_ARTIFACT_DIR_target=dist/target/production
CND_ARTIFACT_NAME_target=miniROV-picLightController.X.production.hex
CND_ARTIFACT_PATH_target=dist/target/production/miniROV-picLightController.X.production.hex
CND_PACKAGE_DIR_target=${CND_DISTDIR}/target/package
CND_PACKAGE_NAME_target=minirov-piclightcontroller.x.tar
CND_PACKAGE_PATH_target=${CND_DISTDIR}/target/package/minirov-piclightcontroller.x.tar
