
/* 
 * File: dig_in.h   
 * Author: E. Martin
 * Comments: 	digital input file, which covers access to four signals
 * 		on the target board, which at this point are define as 
 *		generic signals in the modbus address definitions.
 *    
 * Revision history: Git via BitBucket 
 * 		https://bitbucket.org/mbari/minirov-piclightcontroller 
 * 
 * Copyright: 2016 MBARI, this is proprietary code, all rights reserved.
 *
 */

#ifndef _DIG_IN_H_
#define _DIG_IN_H_

/* INCLUDES */
#include "modbus_registers.h"
 


/**
* Direct read function for access to DI pins.
*
* <p>
* Direct pin read function for a given 
* address. In most cases, the simple DI is read and 
* returned, but in the case of address <em>H2O_SENSE</em>
* we OR the state of two pins connected to leak sensors. 
* In this case some high level control should exist that 
* returns this state in more of an "alarm" manner, i.e. a 
* high state of this input is maintained and another action
* must clear the alarm.
* </p>
*  	
* int discreteReadReg(MB_DISCRETE_INPUT address);
* 
* @param 	address 	modbus register address
* @return  			int value 0 for low input, 1 for high
*				and -1 for ERROR
* 
* 
*/
int discreteReadReg(unsigned int address);

/**
 * Read All digital inputs and return a 16 bit value
 *
 * <p>
 * This function will read all pins and return a 16bit integer
 * representing multiple channels. The order is predetermined 
 * and you can reference the following reference:
 *
 *
 * </p>
 *
 * uint16_t discreteReadAll();
 *
 * @return  int value with bitfield reply for modbus register. 
 *
 */

unsigned int discreteReadAll();

/**
 * Read the board address select pins and return a modbus 
 * address based on their selection. 
 *
 * <p> 
 * Pins RD6 and RD7 are attached to dip switches and
 * their positions are set here for board address.
 * </p>
 *
 * unsigned char hwBoardAddress();
 *
 * @return unsigned char with value corresponding to board 
 * address as set by two input pins on the PIC.
 */

unsigned char hwBoardAddress();
/*
*  Basic IO Functions for DI's 
*
*/ 
#define IO_DigitalIn0_GetValue() _RE1
#define IO_DigitalIn1_GetValue() _RE2
#define IO_CNIn0_GetValue() _RF1
#define IO_CNIn1_GetValue() _RE0
#define IO_H2OSense_GetValue() ((_RE3) | (_RE4))
#define IO_BoardAddr0_GetValue() _RD6
#define IO_BoardAddr1_GetValue() _RD7
#define IO_Uart0Mode_GetValue() _RB2

typedef enum {
    MBADDR_1 = 1,
    MBADDR_2 = 2,
    MBADDR_3 = 3,
    MBADDR_4 = 4,
    MBADDR_DEFAULT = MBADDR_1
    } MBADDR;

#endif


