/****************************************************************************/
/* Copyright 2009 MBARI.                                                    */
/* MBARI Proprietary Information. All rights reserved.                      */
/****************************************************************************/
#ifndef INST_PORT_H
#define INST_PORT_H

#define INST_HOLDING_START  0x02FF
/*#define INST_HOLDING_END    0x01FF*/
#define INST_HOLDING_END    0x04FF

void instPortInit();
void instPortRxTask();
void instPortTxTask();

void instPortClearWriteReq();

unsigned int instPortReadReg(unsigned int address);
void instPortWriteReg(unsigned int address, unsigned int reg);

#endif

