/****************************************************************************/
/* Copyright 2009 MBARI.                                                    */
/* MBARI Proprietary Information. All rights reserved.                      */
/****************************************************************************/
#ifndef MISC_H
#define MISC_H

void EnterCriticalSection( void );
void ExitCriticalSection( void );

#endif
