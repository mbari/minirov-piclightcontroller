# Light Controller Board Checkout Form #

**Serial Number:**        2  
**Part Number:**          100221096rA  
**Manufacture Date:**     February 2016  
  
**Electrical Engineer:**  Chad Kecy  
**Software Engineer:**    Eric Martin  

## Power Stage ##

**Conditions:** 24VDC on J2 and an isolated 24V on J13

  [x] Logic 3.3V          TP2    ( 3.30 V)  
  [x] External +5VDC      U7-3   ( 5.00 V)  
  [x] External +3.3VDC    U6-1   ( 3.30 V)  
  [x] Logic +5V           U29-13 ( 5.40 V)  
  [x] Logic -5V           U29-14 (-5.24V)  
  [x] Isolated +5VDC      U28-13 ( 5.23 V)  
  [x] Isolated -5VDC      U28-14 (-5.23 V)  
      Logic Ground        TP3  
      External Ground     TP4  
      Iso Ground          U28-12  

## Modbus Address ##

Validate the truth table for DIP SW2 below:

| Modbus Address | DIPSW1 | DIPSW2 |
|----------------+--------+--------|
| 1              | OFF    | OFF    |
| 2              | ON     | OFF    |
| 3              | OFF    | ON     |
| 4              | ON     | ON     |

  [x] Modbus Address (`SW2`)
  [x] Modbus Address set to `1`

## Coil Outputs ## 

Test using debug console, modbus coil writes, and modbus coil register writes.

  [x] Isolated Power 0        J14-1  (`ISOPWR_CNTL_0` | `Coil Address 0x01`)  
  [x] Isolated Power 1        J15-1  (`ISOPWR_CNTL_1` | `Coil Address 0x02`)  
  [x] Isolated Power 2        J16-1  (`ISOPWR_CNTL_2` | `Coil Address 0x03`)  
  [x] Isolated Power 3        J17-1  (`ISOPWR_CNTL_3` | `Coil Address 0x04`)  
  [x] Isolated Power 4        J18-1  (`ISOPWR_CNTL_4` | `Coil Address 0x05`)  
  [x] Isolated Power 5        J19-1  (`ISOPWR_CNTL_5` | `Coil Address 0x06`)  
  [x] Ext. Instrument Power 0 J3-1   (`INSTR_CNTL_0`  | `Coil Address 0x07`)  
  [x] Ext. Instrument Power 1 J6-1   (`INSTR_CNTL_1`  | `Coil Address 0x08`)  
  [NI] Digital Output 0              (`DIGITAL_OUT_0` | `Coil Address 0x09`)  
  [NI] Digital Output 1              (`DIGITAL_OUT_1` | `Coil Address 0x0A`)  
  [x] Ground Fault Low Test   K1-1   (`GF_CNTL_LOW`   | `Coil Address 0x0B`)  
  [x] Ground Fault High Test  K2-1   (`GF_CNTL_HIGH`  | `Coil Address 0x0C`)  
  [x] Control Debug State            (`BLINKY_DEBUG`  | `Coil Address 0x0D`)  
  [x] Control Test LED        D2     (`BLINKY_DEBUG`  | `Coil Address 0x0E`)  
  [x] Vicor Control           J7-1   (`VICOR_CNTL`    | `Coil Address 0x10`)  

## Analog Outputs ##

Test all channels for full range output. Set holding registers at 0x0000 and 0xFFFF. 

  [x] LED Dimmer Output 0     J14-3  (`LED_DAC_0` | `Holding Reg 0x200`)  
  [x] LED Dimmer Output 1     J15-3  (`LED_DAC_1` | `Holding Reg 0x201`)  
  [x] LED Dimmer Output 2     J16-3  (`LED_DAC_2` | `Holding Reg 0x202`)  
  [x] LED Dimmer Output 3     J17-3  (`LED_DAC_3` | `Holding Reg 0x203`)  
  [x] LED Dimmer Output 4     J18-3  (`LED_DAC_4` | `Holding Reg 0x204`)  
  [x] LED Dimmer Output 5     J19-3  (`LED_DAC_5` | `Holding Reg 0x205`)  
  
## Water Alarm Test ##

Short pins on J21 or J22 to create a fault. Then turn coil `16` on to clear.

  [x] Water Sense Alarm              (`H2O_ALARM`     | `Discrete Address 0x05`)  
  [x] Clear Water Alarm              (`H2O_ALARM_CLR` | `Coil Address 0x0F`)  
  
## Ground Fault Test ##

Create a fault writing to coils `GF_CNTL_LOW/HIGH`. Create a physical 
fault by shorting J20-2 to either J20-1 or TP4. Measure output on input
register 0x06. 

  [x] No Fault Condition     V=1.535 |    ACounts=520        
  [x] High Side Fault        V=0.720 |    ACounts=20         
  [x] Low Side Fault         V=2.972 |    ACounts=996        

  [x] **Calibration:** `Fault(uA) = (2.05 * Acounts) - 1042`  

## Temp Sensor Analog Input ##

Just calibrate the channel for input voltage accross `U30-3` (`GND`) and `U30-2` (`VOUT`).

  [x] Vout=0.500  |   ACounts=170  
  [x] Vout=1.500  |   ACounts=510  
  [x] Vout=2.999  |   ACounts=1022  

  [x] **Calibration:** `Vout = (0.0029 * Acounts) + 0.0025`  
  
## Communications ##

**Modbus IO**

Initially Set to 19200 8N2.

  [x] RS232 on J10 (JP3 Installed [x])  
  [x] RS485 on J11 (JP3 Removed   [x])  

**Auxillary Serial Ports**

Both set to 9600 8N1

  [x] RS232 on J8 (_This is the Console Port for Bench Testing_)  
  [x] RS232 on J9 (_Currently unused_)  

## Final Notes

This board has the following issues red-lined for revision 2:

- `J20-3` was connected to the wrong net, it should be on `GND_ISO`.  
- Original BOM ordered the _AD5676_, not the _AD5676R_.   
- Future Inputs Are Not Tested (`DIN0, DIN1, CN0, CN1`)  
- Some analog input and output channels were not routed, so they haven't been tested.  
- At present there is no stored config on the PIC, so baud rates are fixed.  

**CERTIFIED BY:** Eric Martin  
**DATE:** Tuesday, March 15th, 2016  
 
