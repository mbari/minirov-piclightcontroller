# Light Controller Board Checkout Form #

> **Serial Number:** 
> **Part Number:**          100221096rA
> **Manufacture Date:**     February 2016
> 
> **Electrical Engineer:**  Chad Kecy
> **Software Engineer:**    Eric Martin

## Power Stage ##

> **Conditions:** 24VDC on J2 and an isolated 24V on J13

[ ] Logic 3.3V          TP2 (xx V)
[ ] External +5VDC      TP  (xx V)
[ ] External +3.3VDC        (xx V)
[ ] Isolated +5VDC          (xx V)
[ ] Isolated -5VDC          (xx V)
    Logic Ground        TP3
    External Ground     TP4

## Modbus Address ##

> Validate the truth table for DIP SW2 below:

  | Modbus Address | DIPSW1 | DIPSW2 |
  ------------------------------------
  | 1              | OFF    | OFF    |
  | 2              | ON     | OFF    |
  | 3              | OFF    | ON     |
  | 4              | ON     | ON     |

[ ] Modbus Address (`SW2`)
[ ] Modbus Address set to `1`

## Coil Outputs ## 

> Test using debug console, modbus coil writes, and modbus coil register writes.

[ ] Isolated Power 0        J14-1  (`ISOPWR_CNTL_0` | `Coil Address 0x01`)
[ ] Isolated Power 1        J15-1  (`ISOPWR_CNTL_1` | `Coil Address 0x02`)
[ ] Isolated Power 2        J16-1  (`ISOPWR_CNTL_2` | `Coil Address 0x03`)
[ ] Isolated Power 3        J17-1  (`ISOPWR_CNTL_3` | `Coil Address 0x04`)
[ ] Isolated Power 4        J18-1  (`ISOPWR_CNTL_4` | `Coil Address 0x05`)
[ ] Isolated Power 5        J19-1  (`ISOPWR_CNTL_5` | `Coil Address 0x06`)
[ ] Ext. Instrument Power 0 J3-1   (`INSTR_CNTL_0`  | `Coil Address 0x07`)
[ ] Ext. Instrument Power 1 J6-1   (`INSTR_CNTL_1`  | `Coil Address 0x08`)
[NI] Digital Output 0              (`DIGITAL_OUT_0` | `Coil Address 0x09`)
[NI] Digital Output 1              (`DIGITAL_OUT_1` | `Coil Address 0x0A`)
[ ] Ground Fault Low Test   K1-1   (`GF_CNTL_LOW`   | `Coil Address 0x0B`)
[ ] Ground Fault High Test  K2-1   (`GF_CNTL_HIGH`  | `Coil Address 0x0C`)
[ ] Control Debug State            (`BLINKY_DEBUG`  | `Coil Address 0x0D`)
[ ] Control Test LED        D2     (`BLINKY_DEBUG`  | `Coil Address 0x0E`)
[ ] Vicor Control           J7-1   (`VICOR_CNTL`    | `Coil Address 0x10`)

## Analog Outputs ##

> Test all channels for full range output. Set holding registers at 0x0000 and 0xFFFF. 

[ ] LED Dimmer Output 0     J14-3  (`LED_DAC_0` | `Holding Reg 0x01`)
[ ] LED Dimmer Output 1     J15-3  (`LED_DAC_1` | `Holding Reg 0x02`)
[ ] LED Dimmer Output 2     J16-3  (`LED_DAC_2` | `Holding Reg 0x03`)
[ ] LED Dimmer Output 3     J17-3  (`LED_DAC_3` | `Holding Reg 0x04`)
[ ] LED Dimmer Output 4     J18-3  (`LED_DAC_4` | `Holding Reg 0x05`)
[ ] LED Dimmer Output 5     J19-3  (`LED_DAC_5` | `Holding Reg 0x06`)

## Water Alarm Test ##

> Short pins on J21 or J22 to create a fault. 

[ ] Water Sense Alarm              (`H2O_ALARM`     | `Discrete Address 0x05`)
[ ] Clear Water Alarm              (`H2O_ALARM_CLR` | `Coil Address 0x0F`)

## Ground Fault Test ##

> Create a fault writing to coils `GF_CNTL_LOW/HIGH`. Create a physical 
> fault by shorting J20-2 to either J20-1 or TP4. Measure output on input
> register 0x06. 

[ ] No Fault Condition     V=xx    |    ACounts=xx       
[ ] High Side Fault        V=xx    |    ACounts=xx       
[ ] Low Side Fault         V=xx    |    ACounts=xx       

[ ] __Calibration:__ `Fault(mA) = (xx * Acounts) + yy`

## Temp Sensor Analog Input ##

> Just calibrate the channel for input voltage accross `U30-3` (`GND`) and `U30-2` (`VOUT`).

[ ] Vout=0.500  |   ACounts=170
    Vout=1.500  |   ACounts=511
    Vout=2.995  |   ACounts=1022

[ ] __Calibration:__ `Vout = (xx * Acounts) + yy`


## Final Notes

This board has the following issues red-lined for revision 2:

- `J20-3` was connected to the wrong net, it should be on `GND_ISO`.
- Original BOM ordered the _AD5676_, not the _AD5676R_. 
- Future Inputs Are Not Tested (`DIN0, DIN1, CN0, CN1`)
- Some analog input and output channels were not routed, so they haven't been tested.


**CERTIFIED BY:** Eric Martin
**DATE:** 
