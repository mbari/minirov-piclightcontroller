/* 
 * File: AD567x.h
 * Author: E. Martin
 * Comments:
 * Revision history: GIT Tracked
 */

#ifndef _AD567X_H
#define	_AD567X_H

#include <xc.h> // include processor files - each processor file is guarded.  

//#include <stdint.h>


#include "mcc_generated_files/spi1.h"

#ifdef	__cplusplus
extern "C" {
#endif /* __cplusplus */

    /** AD567X_CMD
     * Command definitions for the 4bit command portion of the SPI message
     */
    typedef enum {
        DACCMD_NOOP = 0x00,
        DACCMD_WRITEREG = 0x01,
        DACCMD_UPDATEFROMREG = 0x02,
        DACCMD_WRITEREGNUPDATE = 0x03,
        DACCMD_POWERUPDOWN = 0x04,
        DACCMD_LDACREG = 0x05,
        DACCMD_SOFTRST = 0x06,
        DACCMD_INTREFREG = 0x07,
        DACCMD_DAISYCHAINEN = 0x08,
        DACCMD_READBACKEN = 0x09,
        DACCMD_UPDALLREG = 0x0A,
        DACCMD_UPDALLDAC = 0x0B
    } AD567X_CMD;

    /** AD567X_CHAN 
     * Channel definitions for the 4bit channel address of the SPI message
     */

    typedef enum {
        DACCHAN_0 = 0x00,
        DACCHAN_1 = 0x01,
        DACCHAN_2 = 0x02,
        DACCHAN_3 = 0x03,
        DACCHAN_4 = 0x04,
        DACCHAN_5 = 0x05,
        DACCHAN_6 = 0x06,
        DACCHAN_7 = 0x07
    } AD567X_CHAN;

    /**
     *     void AD567X_Initialize();
     * function: initialize the driver
     */
    void AD567X_Initialize();

    /**
     *     void AD567X_TasksTransmit();
     * Function: do all transmit tasks. 
     */
    void AD567X_TasksTransmit();


    /**
     *  void AD567X_AssembleDataRegister(uint8_t command, uint8_t address, uint16_t value);    
     * 
     * function: assemble the generic message associated with many commmands and
     * enqueue it for transmission to the IC. 
     * 
     * @param command
     * @param address
     * @param value
     */
    void AD567X_AssembleDataRegister(uint8_t command, uint8_t address, uint16_t value);

    /**
     * void AD567X_AssembleDCENRegister(uint8_t command, uint8_t address, uint16_t value);    
     * 
     * function: assemble the 24-bit register for daisy chain configuration and 
     * enabling. Then enqueue it. 
     * @param command
     * @param address
     * @param value
     */
    void AD567X_AssembleDCENRegister(uint8_t command, uint8_t address, uint16_t value);

    /**
     * void AD567X_AssemblePowerUpDownRegister(uint8_t command, uint8_t address, uint16_t value);    
     * @param command
     * @param address
     * @param value
     */
    void AD567X_AssemblePowerUpDownRegister(uint8_t command, uint8_t address, uint16_t value);

    /**
     *     void AD567X_WriteQueue();
     * 
     * function: write out the existing queue.
     */
    void AD567X_WriteQueue();
    
    /**
     * Section: Direct Command Assembly for Each Available Command
     * 
     * @Summary: Not all commands will be implemented. Look for todo's
     */

    /**
     * writeToInputRegister()
     * @param channel
     * @param value
     */
    void AD567X_WriteToInputRegister(AD567X_CHAN channel, uint16_t value);

    /**
     *  void AD567X_UpdateDACRegister(AD567X_CHAN channel);
     * 
     * Function: update DAC register <channel> with contents of input register
     * <channel>
     * 
     * @param channel
     */
    void AD567X_UpdateDACRegister(AD567X_CHAN channel);


    /**
     * void writeToAndUpdateChannel(uint8_t channel, uint16_t value);
     * 
     * Function : update and immediately change channel
     * 
     * @param channel: must be a valid address
     * @param value: unsigned scale output for full range 
     */
    void AD567X_WriteToAndUpdateChannel(AD567X_CHAN channel, uint16_t value);

    /**
     * powerUpDown()
     * 
     * Function: issue the command to power up/down the DAC
     */
    void AD567X_PowerUpDown();

    /**
     * void hardwareLDACMaskRegister();
     * 
     * function: set the LDAC mask register for individual channel response
     * to the LDAC pin
     */
    void AD567X_HardwareLDACMaskRegister();

    /**
     * void AD567X_softwareReset();
     * 
     * function: software controlled power-on reset
     */
    void AD567X_SoftwareReset();

    /**
     * void AD567X_daisyChainEnable();
     * 
     * function: enable daisy chain functionality (later TODO)
     */
    void AD567X_DaisyChainEnable();

    /**
     * void AD567X_readBackEnable();
     * 
     * function: set up the readback register (later TODO)
     */
    uint16_t AD567X_ReadBackEnable(AD567X_CHAN channel);

    /**
     * void AD567X_updateAllInputRegisters(uint16_t value);
     * 
     * function: update the input registers with the input data
     * @param value: 16-bit scale value
     */
    void AD567X_updateAllInputRegisters(uint16_t value);


    /**
     * void AD567X_UpdateAllChannels(uint16_t value);
     * 
     * function: update all channels of the DAC register and input register with
     * the input data
     * @param value: 16-bit scale value. 
     */
    void AD567X_UpdateAllChannels(uint16_t value);

    /**
     * Setup internal Reference state, 0 for 
     * @param on
     */
    void AD567X_SetupInternalRef(uint16_t on);




#ifdef	__cplusplus
}
#endif /* cplusplus */

#endif	/* _AD567X_H */

